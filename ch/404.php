<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Страница не найдена");
?>
<div class="not-found">
    <div class="not-found__text">
        <div class="center"><span class="text text_big">404</span><span
                    class="text text_small"><?= GetMessage('ERROR') ?></span>
        </div>
    </div>
    <div class="creator not-found__creator">
        <div class="creator-text"><?= GetMessage('DEVELOPMENT') ?></div>
        <a class="creator-logo creator-logo_white" href="https://ilartech.com/" target="_blank"
           style="background-image: url(<?= SITE_STYLE_PATH ?>/img/minified-svg/ilar-pro-white.svg)"></a>
        <a class="creator-logo creator-logo_black" href="https://ilartech.com/" target="_blank"
           style="background-image: url(<?= SITE_STYLE_PATH ?>/img/minified-svg/ilar-pro-black.svg)"></a>
    </div>
    <div class="not-found__content">
        <div class="not-found__line">
            <div class="not-found__label"><?= GetMessage('PAGE_NOT_FOUND') ?></div>
            <div class="not-found__heading heading heading_level-1"><?= GetMessage('INCORRECT_ADDRESS') ?></div>
        </div>
        <a class="main-button" href="<?= SITE_DIR ?>"><span
                    class="main-button__text"><?= GetMessage('TO_MAIN') ?></span></a>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
