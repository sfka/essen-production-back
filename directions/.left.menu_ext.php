<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;
if (CModule::IncludeModule("iblock")) {

    $IBLOCK_ID = 1; // инфоблок с элементами

    $arOrder = ["SORT" => "ASC"];
    $arSelect = ["ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL"];
    $arFilter = ["IBLOCK_ID" => $IBLOCK_ID, "ACTIVE" => "Y"];
    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $aMenuLinksExt[] = [
            $arFields['NAME'],
            $arFields['DETAIL_PAGE_URL'],
            [],
            [],
            "",
        ];
    }
}

$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);
?>