<?
define("SITE_STYLE_PATH", "/local/styles");
define("SITE_INCLUDE_PATH", "/local/include");
define("SITE_USER_CLASS_PATH", "/local/php_interface/user_class");
define("SITE_AJAX_PATH", "/local/ajax");

define("IBLOCK_DIRECTIONS", "1");
define("IBLOCK_LEADERSHIP", "11");
define("IBLOCK_FORM_VACANCY", "15");
define("IBLOCK_FORM_CONTACTS", "19");

CModule::AddAutoloadClasses("", [
    '\IL\Iblock' => SITE_USER_CLASS_PATH . '/classIblock.php',
    '\IL\Utilities' => SITE_USER_CLASS_PATH . '/classUtilities.php',
    '\IL\Settings' => SITE_USER_CLASS_PATH . '/classSettings.php',
    '\IL\Catalog' => SITE_USER_CLASS_PATH . '/classCatalog.php',
    '\IL\FormFeedback' => SITE_USER_CLASS_PATH . '/classFormFeedback.php',
    '\IL\FormVacancy' => SITE_USER_CLASS_PATH . '/classFormVacancy.php',
    '\IL\FormContacts' => SITE_USER_CLASS_PATH . '/classFormContacts.php',
]);

AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);

function _Check404Error(){
    if (defined('ERROR_404') && ERROR_404 == 'Y') {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        include $_SERVER['DOCUMENT_ROOT'] . '/local/templates/index/header.php';
        include $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . '404.php';
        include $_SERVER['DOCUMENT_ROOT'] . '/local/templates/index/footer.php';
    }
}



//\Bitrix\Main\EventManager::getInstance()->addEventHandler("catalog", "OnProductAdd", ['\IL\Handlers', 'OnSetProps']);