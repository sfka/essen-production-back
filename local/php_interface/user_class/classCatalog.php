<?

namespace IL;

use \Bitrix\Main\Loader;

/**
 * Класс для работы с каталогорм
 * Class Catalog
 * @package IL
 */
class Catalog {

    /**
     * Возвращает список разделов в виде дерева
     * @param $iblockID
     * @return bool|mixed
     */
    public static function getSectTreeList($iblockID) {
        if (!Loader::includeModule('iblock')) return false;

        $arFilter = [
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => IntVal($iblockID),
            'GLOBAL_ACTIVE' => 'Y',
        ];
        $arSelect = [
            'IBLOCK_ID',
            'ID',
            'NAME',
            'DEPTH_LEVEL',
            'IBLOCK_SECTION_ID',
        ];
        $arOrder = [
            'DEPTH_LEVEL' => 'ASC',
            'SORT' => 'ASC',
        ];
        $rsSections = \CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
        $sectionLinc = [];
        $arResult['ROOT'] = [];
        $sectionLinc[0] = &$arResult['ROOT'];
        while ($arSection = $rsSections->GetNext()) {
            $sectionLinc[(int)$arSection['IBLOCK_SECTION_ID']]['CHILD'][$arSection['ID']] = $arSection;
            $sectionLinc[$arSection['ID']] = &$sectionLinc[(int)$arSection['IBLOCK_SECTION_ID']]['CHILD'][$arSection['ID']];
        }
        unset($sectionLinc);
        $arResult['ROOT'] = $arResult['ROOT']['CHILD'];
        return $arResult['ROOT'];
    }

    /**
     * Возвращает информацию о разделе по ID
     * @param $iblockID
     * @param $sectionID
     * @param array $arOrder
     * @return array|bool
     */
    public static function getSectionsByID($iblockID, $sectionID, $arOrder = []) {
        if (!Loader::includeModule('iblock')) return false;

        $arRes = [];
        $arFilter = [
            'ACTIVE ' => 'Y',
            'GLOBAL_ACTIVE' => 'Y',
            'IBLOCK_ID' => $iblockID,
            '=ID' => $sectionID,
        ];
        $arSelect = [];
        $rsSect = \CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect, false);
        while ($obSect = $rsSect->GetNext()) {
            $arRes[] = $obSect;
        }
        return $arRes;
    }

    /**
     * Возвращает название раздела по ID
     * @param $iblockID
     * @param $sectionID
     * @return array|bool
     */
    public static function getSectionNameByID($iblockID, $sectionID) {
        if (!Loader::includeModule('iblock')) return false;

        $arFilter = [
            'ACTIVE ' => 'Y',
            'GLOBAL_ACTIVE' => 'Y',
            'IBLOCK_ID' => $iblockID,
            '=ID' => $sectionID,
        ];
        $arSelect = ['NAME'];
        $arRes = \CIBlockSection::GetList([], $arFilter, false, $arSelect, false)->Fetch();
        return $arRes['NAME'];
    }

    /**
     * Возвращает параметры родительского раздела
     * @param $url
     * @return array|bool
     */
    public static function getParentSectParams($url) {
        if (!Loader::includeModule('iblock')) return false;

        $arResult = [];

        $arLinks = array_diff(explode('/', $url), ['']);
        $arLink = array_pop($arLinks);
        if (count($arLinks) > 0) {
            $sectUrl = implode('/', $arLinks);
        } else {
            $sectUrl = $arLink;
        }

        //Получим название раздела
        $sSectionName = '';
        $sPath = $_SERVER['DOCUMENT_ROOT'] . '/' . $sectUrl . '/' . '.section.php';
        @include($sPath);//Теперь в переменной $sSectionName содержится название раздела
        $arResult['NAME'] = $sSectionName;
        $arResult['URL'] = SITE_DIR . $sectUrl . '/';
        $arResult['PARENT_SECT'] = current($arLinks);
        $arResult['CHILD_SECT'] = end(array_diff(explode('/', $url), ['']));

        return $arResult;
    }

    /**
     * Возвращает разделы инфоблока с элементами
     * @param $iblockID
     * @param bool $active
     * @param array $params
     * @return array|bool
     */
    public static function getSectionList($iblockID, $active = true, $params = []) {
        if (!Loader::includeModule('iblock')) return false;

        $arResult = [];
        $arOrder = ['SORT' => 'ASC'];
        $arFilter = [
            'IBLOCK_ID' => $iblockID,
            "ACTIVE" => "Y",
            "INCLUDE_SUBSECTIONS" => "Y",
        ];
        $arFilter = array_merge($arFilter, $params);
        $arSelect = [];
        $rsSect = \CIBlockSection::GetList($arOrder, $arFilter, true, $arSelect, false);
        while ($arSect = $rsSect->GetNext()) {
            if ($active) {
                if ($arSect['ELEMENT_CNT'] > 0) {
                    $arResult[$arSect['ID']] = $arSect;
                    $arResult[$arSect['ID']]['ITEMS'] = self::getElementList($iblockID, ["IBLOCK_SECTION_ID" => $arSect['ID']]);
                }
            } else {
                $arResult[$arSect['ID']] = $arSect;
                $arResult[$arSect['ID']]['ITEMS'] = self::getElementList($iblockID, ["IBLOCK_SECTION_ID" => $arSect['ID']]);
            }
        }
        return $arResult;
    }

    /**
     * Возвращает список элементов со свойствами
     * @param $iblockID
     * @param $arParams
     * @param array $order
     * @return array|bool
     */
    public static function getElementList($iblockID, $arParams = [], $order = ['SORT' => 'ASC']) {
        if (!Loader::includeModule('iblock')) return false;

        $arResult = [];
        $arOrder = $order;
        $arSelect = [];
        $arFilter = [
            "IBLOCK_ID" => IntVal($iblockID),
            "ACTIVE" => "Y",
        ];
        $arFilter = array_merge($arFilter, $arParams);
        $res = \CIBlockElement::GetList($arOrder, $arFilter, false, [], $arSelect);
        while ($ob = $res->GetNextElement()) {

            $arFields = $ob->GetFields();
            $arFields['PROPERTIES'] = [];
            $arProps = $ob->GetProperties();

            $arFields['PROPERTIES'] = $arProps;
            array_push($arResult, $arFields);
        }
        return $arResult;
    }

    /**
     * Получим название элемента по ID
     * @param $iblockID
     * @param $elementID
     * @param array $arProps
     * @return array|bool
     */
    public static function getElementNameByID($iblockID, $elementID, $arProps = []) {
        if (!Loader::includeModule('iblock')) return false;

        $arSelect = ["NAME"];
        $arFilter = [
            "IBLOCK_ID" => IntVal($iblockID),
            "ID" => $elementID,
            "ACTIVE" => "Y",
        ];
        $arFilter = array_merge($arFilter, $arProps);
        $res = \CIBlockElement::GetList([], $arFilter, $arSelect, false, [])->Fetch();
        return $res['NAME'];
    }

}