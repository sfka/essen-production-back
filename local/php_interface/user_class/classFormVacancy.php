<?

namespace IL;

use \Bitrix\Main\Loader;

/**
 * Класс для работы с формой заявка на услугу
 * Class FormFeedback
 * @package IL
 */
class FormVacancy extends Iblock
{

    /**
     * Добавляет элемент с данными из формы в ИБ
     * @param $arFields
     * @return bool|string
     */
    public function add($arFields)
    {
        if (!Loader::includeModule('iblock')) return false;

        $el = new \CIBlockElement;
        $arLoadProductArray = [
            "IBLOCK_ID" => $this->iblockID,
            "DATE_ACTIVE_FROM" => date('d.m.Y H:i:s'),
            "PROPERTY_VALUES" => [
                'SURNAME' => $arFields['surname'],
                'NAME' => $arFields['name'],
                'PATRONYMIC' => $arFields['patronymic'],
                'DATE' => $arFields['date'],
                'EMAIL' => $arFields['email'],
                'PHONE' => $arFields['phone'],
                'VACANCY' => $arFields['vacancy'],
            ],
            "NAME" => "Заявка от " . date('d.m.Y H:i:s'),
            "ACTIVE" => "N",
            "TYPE" => "html",
        ];
        if (isset($arFields['FILES'])) {
            $error = false;
            $ext = pathinfo($_FILES["file"]['name'], PATHINFO_EXTENSION);
            $allowed = explode(', ', $arFields['type_file']);
            if (!in_array($ext, $allowed)) {
                $error = true;
            } else {
                $file = [
                    'name' => $_FILES["file"]['name'],
                    'size' => $_FILES["file"]['size'],
                    'tmp_name' => $_FILES["file"]['tmp_name'],
                    'type' => $_FILES["file"]['type'],
                ];
                $arFiles[] = [
                    'VALUE' => $file,
                    'DESCRIPTION' => '',
                ];
            }
            if ($error) {
                return 'Неверный тип файлов, требуется ' . $arFields['type_file'];
            }
            $arLoadProductArray["PROPERTY_VALUES"]["FILE"] = $arFiles;
        }
        $fieldText = '';
        foreach ($arLoadProductArray['PROPERTY_VALUES'] as $key => $values) {
            if ($values != '') {
                $resName = \CIBlockProperty::GetByID($key, $this->iblockID, false)->GetNext();
                $name = $resName['NAME'];
                $fieldText .= $name . ': ' . $values . '<br>';
            }
        }
        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            $arEventFields = [
                "FORM_NAME" => "Заявка на услугу",
                "TEXT" => $fieldText,
            ];
            \CEvent::Send("FORMS", SITE_ID, $arEventFields, 'N', 31);
            return true;
        } else {
            return $el->LAST_ERROR;
        }
    }

}