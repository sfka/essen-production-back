<?

namespace IL;

/**
 * Class Iblock
 * @package IL
 */
class Iblock {
    /**
     * @var $iblockID
     */
    protected $iblockID;
    /**
     * @var $defaultIblockID
     */
    public static $defaultIblockID = 0;

    /**
     * Iblock constructor.
     * @param $iblockID
     */
    public function __construct($iblockID) {
        if ($iblockID > 0) {
            $this->iblockID = intval($iblockID);
        } elseif ($this->defaultIblockID > 0) {
            $this->iblockID = self::$defaultIblockID;
        } else {
            return false;
        }
        $this->iblockID = $iblockID;
    }

}