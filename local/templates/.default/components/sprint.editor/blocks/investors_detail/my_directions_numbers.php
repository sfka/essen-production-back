<? /** @var $block array */ ?>
<? if ($block['items']): ?>
    <div class="items-data">
        <? foreach ($block['items'] as $item): ?>
            <div class="item-data">
                <div class="inner-data">
                    <div class="count-data"><?= $item['number'] ?></div>
                    <div class="desc-data"><?= $item['title'] ?></div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>