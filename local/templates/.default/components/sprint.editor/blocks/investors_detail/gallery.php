<? /**
 * @var $block array
 * @var $this  SprintEditorBlocksComponent
 */ ?><?
$images = Sprint\Editor\Blocks\Gallery::getImages(
    $block, [
    'width' => 1024,
    'height' => 1024,
    'exact' => 0,
], [
        'width' => 1500,
        'height' => 1500,
        'exact' => 0,
    ]
);
?>
<? if (!empty($images)): ?>
    <div class="gallery">
        <div class="gallery-wrap">
            <? foreach ($images as $image): ?>
                <div class="gallery-img">
                    <a data-fancybox="gallery" class="fancy" rel="media-gallery"
                       href="<?= $image['DETAIL_SRC'] ?>">
                        <img class="lazy" alt="<?= $image['DESCRIPTION'] ?>" data-src="<?= $image['SRC'] ?>">
                    </a>
                </div>
            <? endforeach; ?>
        </div>
        <div class="gallery__prev-btn gallery-btn">
            <svg class="gallery-btn__icon" width="24" height="16">
                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-left"></use>
            </svg>
        </div>
        <div class="gallery__next-btn gallery-btn">
            <svg class="gallery-btn__icon" width="24" height="16">
                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
            </svg>
        </div>
    </div>
<? endif; ?>
