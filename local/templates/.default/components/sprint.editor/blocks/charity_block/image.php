<? /** @var $block array */ ?><?
$image = Sprint\Editor\Blocks\Image::getImage(
    $block, [
        'width' => 1024,
        'height' => 768,
        'exact' => 0,
        //'jpg_quality' => 75
    ]
);
?><? if ($image): ?>
    <div class="gallery">
        <div class="gallery-img">
            <a class="gallery-img" href="javascript:;" data-fancybox="gallery"
               data-options="{&quot;src&quot; : &quot;<?= $image['SRC'] ?>&quot;}">
                <img class="lazy" alt="<?= $image['DESCRIPTION'] ?>" data-src="<?= $image['SRC'] ?>">
            </a>
            <? if ($image['DESCRIPTION']): ?>
                <div class="gallery-img__desc">
                    <h3><?= $image['DESCRIPTION'] ?></h3>
                </div>
            <? endif; ?>
        </div>
    </div>
<? endif; ?>
