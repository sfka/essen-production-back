<? /** @var $block array */ ?>
<div class="content__date">
    <? foreach ($block['elements'] as $item): ?>
        <div class="content__date-item" data-href="#<?= $item['anchor'] ?>">
            <h5><?= $item['text'] ?></h5><span><?= $item['long_text'] ?></span>
            <div class="more-arrow content__date-progress">
                <svg class="icon" width="16" height="16">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#scroll-up"></use>
                </svg>
            </div>
        </div>
    <? endforeach; ?>
</div>
