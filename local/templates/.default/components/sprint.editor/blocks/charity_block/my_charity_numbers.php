<? /** @var $block array */
?>
<div class="content__numeral">
    <div class="content__numeral-info">
        <div class="content__numeral-column content__numeral-count">
            <div class="count"><?= $block['valueCount'] ?></div>
            <div class="title"><?= $block['valueTitle'] ?></div>
        </div>
        <? if ($block['valueDesc']): ?>
            <div class="content__numeral-column content__numeral-text">
                <p><?= $block['valueDesc'] ?></p>
            </div>
        <? endif; ?>
    </div>
    <? if ($block['valueQuote']): ?>
        <div class="quote">
            <figcaption>
                <blockquote><?= $block['valueQuote'] ?></blockquote>
            </figcaption>
        </div>
    <? endif; ?>
</div>
