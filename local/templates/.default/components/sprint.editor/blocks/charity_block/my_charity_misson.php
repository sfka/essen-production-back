<? /** @var $block array */ ?><?

$text = Sprint\Editor\Blocks\Text::getValue($block['text']);
$imageLeft = Sprint\Editor\Blocks\Image::getImage(
    $block['image-left'], [
        'width' => 350,
        'height' => 350,
        'exact' => 0,
    ]
);
$imageRight = Sprint\Editor\Blocks\Image::getImage(
    $block['image-right'], [
        'width' => 350,
        'height' => 350,
        'exact' => 0,
    ]
);
$imageLight = Sprint\Editor\Blocks\Image::getImage(
    $block['image-light'], [
        'width' => 350,
        'height' => 350,
        'exact' => 0,
    ]
);
$imageDark = Sprint\Editor\Blocks\Image::getImage(
    $block['image-dark'], [
        'width' => 350,
        'height' => 350,
        'exact' => 0,
    ]
);
$text = Sprint\Editor\Blocks\Text::getValue($block['text']);
?>
<div class="content__circle">
    <div class="content__circle-item content__circle-item_left">
        <img src="<?= $imageLeft['SRC'] ?>" alt="">
    </div>
    <div class="content__circle-item content__circle-item_right">
        <img src="<?= $imageRight['SRC'] ?>" alt="">
    </div>
</div>
<div class="content__top">
    <div class="content__logo content__logo_width-2">
        <div class="logo-reverse logo-top">
            <div class="light">
                <img src="<?= $imageLight['SRC'] ?>" alt="<?= $imageLight['DESCRIPTION'] ?>">
            </div>
            <div class="dark">
                <img src="<?= $imageDark['SRC'] ?>" alt="<?= $imageDark['DESCRIPTION'] ?>">
            </div>
        </div>
    </div>
    <div class="content__cite cite"><?= $text ?></div>
</div>