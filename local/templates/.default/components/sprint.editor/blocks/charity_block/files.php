<? /**
 * @var $block array
 * @var $this  SprintEditorBlocksComponent
 */ ?><?
?>
<? if (!empty($block['files'])): ?>
    <? if (count($block['files']) > 1): ?>
        <div class="slider">
            <div class="slider-items swiper-container">
                <div class="slider-wrap swiper-wrapper">
                    <? foreach ($block['files'] as $item): ?>
                        <div class="column-wrap swiper-slide">
                            <div class="slider-item">
                                <a class="docs-item" href="<?= $item['file']['SRC'] ?>" data-type="small"
                                   target="_blank">
                                    <div class="docs-item__inner">
                                        <div class="docs-item__info">
                                            <div class="icon-info">
                                                <svg class="icon" width="16" height="20">
                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#doc"></use>
                                                </svg>
                                            </div>
                                            <? $fileType = explode('.', $item['file']['ORIGINAL_NAME'])[1]; ?>
                                            <div class="title-info"><?= strtoupper($fileType); ?>
                                                , <?= CFile::FormatSize($item['file']['FILE_SIZE']) ?></div>
                                        </div>
                                        <div class="docs-item__text"><?= ($item['desc'] ? $item['desc'] : $item['file']['ORIGINAL_NAME']) ?></div>
                                        <div class="docs-item__btn card__btn">
                                            <svg class="card__btn-icon" width="36" height="24">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
            <div class="slider__controls">
                <div class="slider-controls">
                    <div class="slider-btn slider__btn-prev">
                        <svg class="slider-btn__icon" width="18" height="12">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-left"></use>
                        </svg>
                    </div>
                    <div class="slider-btn slider__btn-next">
                        <svg class="slider-btn__icon" width="18" height="12">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    <? else: ?>
        <? foreach ($block['files'] as $item): ?>
            <? $fileType = explode('.', $item['file']['ORIGINAL_NAME'])[1]; ?>
            <div class="content__file">
                <a class="main-button magnetic-wrap" href="<?= $item['file']['SRC'] ?>">
                    <span class="main-button__text"><?= ($item['desc'] ? $item['desc'] : $item['file']['ORIGINAL_NAME']) ?> (<?= strtoupper($fileType) ?>, <?= CFile::FormatSize($item['file']['FILE_SIZE']) ?>)</span>
                </a>
            </div>
        <? endforeach; ?>
    <? endif; ?>
<? endif; ?>
