<? /** @var $block array */ ?>
<? if ($block['items']): ?>
    <div class="content__counts">
        <? foreach ($block['items'] as $item): ?>
            <div class="content__counts-item">
                <div class="inner-item">
                    <div class="center">
                        <div class="count-item"><?= $item['number'] ?></div>
                        <div class="desc-item"><?= $item['title'] ?></div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>