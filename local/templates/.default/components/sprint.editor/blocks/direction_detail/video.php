<? /** @var $block array */ ?><?


$preview = Sprint\Editor\Blocks\Image::getImage($block['preview'], [
    'width' => 1024,
    'height' => 768,
    'exact' => 0,
    //'jpg_quality' => 75
]);

?>
<div class="photo offset-big">
    <div class="photo__img">
        <div class="photo__video">
            <?= Sprint\Editor\Blocks\Video::getHtml($block) ?>
            <div class="play-video">
                <? if ($preview['SRC']): ?>
                    <img class="play-video__poster lazy" data-src="<?= $preview['SRC'] ?>" alt="<?= $preview['DESCRIPTION'] ?>">
                <? endif; ?>
                <svg class="icon" width="100" height="100">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#play"></use>
                </svg>
            </div>
        </div>
    </div>
    <div class="photo__desc offset-center"><?= $preview['DESCRIPTION'] ?></div>
</div>
<script>
    $('iframe').each(function () {
        $(this).attr({
            'data-type': 'iframe',
            'allow': 'accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
        })
    });
</script>