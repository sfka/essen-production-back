<? /** @var $block array */ ?><?

$text = Sprint\Editor\Blocks\Text::getValue($block['text']);
$image1 = Sprint\Editor\Blocks\Image::getImage(
    $block['image1'], [
        'width' => 320,
        'height' => 240,
        'exact' => 0,
    ]
);
$image2 = Sprint\Editor\Blocks\Image::getImage(
    $block['image2'], [
        'width' => 320,
        'height' => 240,
        'exact' => 0,
    ]
);
?>
<div class="img-programs">
    <div class="img">
        <div class="logo-reverse">
            <div class="light">
                <img class="lazy" data-src="<?= $image1['SRC'] ?>" alt="<?= $image1['DESCRIPTION'] ?>">
            </div>
            <div class="dark">
                <img class="lazy" data-src="<?= $image2['SRC'] ?>" alt="<?= $image2['DESCRIPTION'] ?>">
            </div>
        </div>
    </div>
    <div class="more-text">
        <div class="inner">
            <?= $text ?>
        </div>
        <div class="more-text__btn">
            <div class="more-link" data-text="<?= GetMessage('SHOW_ALL') ?>"><?= GetMessage('SHOW_ALL') ?></div>
            <div class="more-arrow">
                <svg class="icon" width="16" height="16">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#scroll-up"></use>
                </svg>
            </div>
        </div>
    </div>
</div>
