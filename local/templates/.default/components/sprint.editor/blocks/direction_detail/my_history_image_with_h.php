<? /** @var $block array */ ?><?

$image = Sprint\Editor\Blocks\Image::getImage(
    $block['image'], []
);
?>
<div class="gallery history__gallery my_history_image_with_h">
    <div class="gallery-img">
        <img class="lazy" alt="<?= $image['DESCRIPTION'] ?>" data-src="<?= $image['SRC'] ?>">
        <div class="gallery-img__desc desc">
            <div class="inner">
                <? if (!empty($block['htag']['anchor'])): ?><a name="<?= $block['htag']['anchor'] ?>"></a><? endif; ?>
                <<?= $block['htag']['type'] ?> class="date"><?= $block['htag']['value'] ?></<?= $block['htag']['type'] ?>>
                <span class="title"><?= $image['DESCRIPTION'] ?></span>
            </div>
        </div>
    </div>
</div>
