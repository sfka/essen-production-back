<? /** @var $block array */ ?><?

$text = Sprint\Editor\Blocks\Text::getValue($block['text']);
$image3 = Sprint\Editor\Blocks\Image::getImage(
    $block['image3'], [
        'width' => 600,
        'height' => 600,
        'exact' => 0,
    ]
);
?>
<figure class="quote">
    <div class="quote__inner">
        <div class="quote__img circle-img">
            <img class="boss lazy" data-src="<?= $image3['SRC'] ?>" alt="">
        </div>
        <figcaption>
            <blockquote><?=$text ?></blockquote>
            <p><?= $block['valueCount'] ?></p>
            <cite><?= $block['valueTitle'] ?></cite>
        </figcaption>
    </div>
</figure>