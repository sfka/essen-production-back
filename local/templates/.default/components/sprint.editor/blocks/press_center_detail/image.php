<? /** @var $block array */ ?><?
$image = Sprint\Editor\Blocks\Image::getImage(
    $block, [
        'width' => 1024,
        'height' => 768,
        'exact' => 0,
        //'jpg_quality' => 75
    ]
);
?><? if ($image): ?>
    <div class="photo">
        <div class="photo__img">
            <img class="lazy" data-src="<?= $image['SRC'] ?>" alt="<?= $image['DESCRIPTION'] ?>">
        </div>
        <div class="photo__desc"><?= $image['DESCRIPTION'] ?></div>
    </div>
<? endif; ?>
