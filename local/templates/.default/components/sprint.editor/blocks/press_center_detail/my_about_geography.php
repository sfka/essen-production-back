<? /** @var $block array */ ?><?

$text = Sprint\Editor\Blocks\Text::getValue($block['text']);
$image = Sprint\Editor\Blocks\Image::getImage(
    $block['image'], [
        'width' => 1400,
        'height' => 1400,
        'exact' => 0,
    ]
);
?>
<div class="content__map">
    <div class="map">
        <img class="lazy" data-src="<?= $image['SRC'] ?>" alt="<?= $image['DESCRIPTION'] ?>">
    </div>
</div>
<? if ($block['items']): ?>
    <div class="content__items">
        <? foreach ($block['items'] as $item): ?>
            <? if ($item['number']): ?>
                <div class="content__items-item">
                    <div class="counter">
                        <div class="counter__num"><?= $item['number'] ?></div>
                        <div class="counter__title"><?= $item['title'] ?></div>
                        <div class="counter__text"><?= $item['description'] ?></div>
                    </div>
                </div>
            <? endif; ?>
        <? endforeach; ?>
    </div>
<? endif; ?>
