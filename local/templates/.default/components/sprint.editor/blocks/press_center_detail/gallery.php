<? /**
 * @var $block array
 * @var $this  SprintEditorBlocksComponent
 */ ?><?
$images = Sprint\Editor\Blocks\Gallery::getImages(
    $block, [
    'width' => 400,
    'height' => 400,
    'exact' => 0,
], [
        'width' => 1500,
        'height' => 1500,
        'exact' => 0,
    ]
);
?>
<? if (!empty($images)): ?>
    <h2><?= GetMessage('PHOTO_GALLERY') ?></h2>
    <div class="photo-gallery">
        <div class="photo-gallery__items">
            <? foreach ($images as $image): ?>
                <div class="photo-gallery__column">
                    <a class="photo-gallery__item" href="javascript:void(0);" data-fancybox="gallery"
                       data-options="{&quot;src&quot; : &quot;<?= $image['DETAIL_SRC'] ?>&quot;}">
                        <img alt="<?= $image['DESCRIPTION'] ?>" src="<?= $image['SRC'] ?>">
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? endif; ?>
