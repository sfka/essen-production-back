<? /**
 * @var $block array
 * @var $this  SprintEditorBlocksComponent
 */ ?><?
?>
<? \IL\Utilities::DB($block); ?>
<? if (!empty($block['files'])): ?>
    <div class="docs offset-small">
        <div class="docs-table">
            <div class="docs-table__thead">
                <div class="th"><?= GetMessage('DATE_FILE') ?>:</div>
                <div class="th"><?= GetMessage('NAME') ?>:</div>
                <div class="th"><?= GetMessage('DOWNLOAD') ?></div>
            </div>
            <div class="docs-list">
                <? foreach ($block['files'] as $item): ?>
                    <a class="docs-item" href="<?= $item['file']['SRC'] ?>" data-type="big" target="_blank">
                        <div class="docs-item__inner">
                            <div class="docs-item__info">
                                <div class="icon-info">
                                    <svg class="icon" width="16" height="20">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#doc"></use>
                                    </svg>
                                </div>
                                <? $fileType = explode('.', $item['file']['ORIGINAL_NAME'])[1]; ?>
                                <div class="title-info"><?= strtoupper($fileType) ?>, <?= CFile::FormatSize($item['file']['FILE_SIZE']) ?></div>
                                <div class="date-info"><?= $item['date'] ?></div>
                            </div>
                            <div class="docs-item__text"><?= $item['desc'] ?></div>
                            <div class="docs-item__btn card__btn">
                                <svg class="card__btn-icon" width="36" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                                </svg>
                            </div>
                        </div>
                    </a>
                <? endforeach; ?>
            </div>
        </div>
    </div>
<? endif; ?>