<? /** @var $block array */ ?><?
$image = Sprint\Editor\Blocks\Image::getImage(
    $block, [
        'width' => 1024,
        'height' => 768,
        'exact' => 0,
        //'jpg_quality' => 75
    ]
);
?><? if ($image): ?>
    <div class="gallery">
        <div class="gallery-img">
            <img class="lazy" data-src="<?= $image['SRC'] ?>" alt="<?= $image['DESCRIPTION'] ?>">
            <div class="gallery-img__desc">
                <h3><?= $image['DESCRIPTION'] ?></h3>
            </div>
        </div>
    </div>
<? endif; ?>
