<? /** @var $block array */ ?><?

$text = Sprint\Editor\Blocks\Text::getValue($block['text']);
$imageLeft = Sprint\Editor\Blocks\Image::getImage(
    $block['image-left'], [
        'width' => 350,
        'height' => 350,
        'exact' => 0,
    ]
);
$imageRight = Sprint\Editor\Blocks\Image::getImage(
    $block['image-right'], [
        'width' => 350,
        'height' => 350,
        'exact' => 0,
    ]
);
$imageLight = Sprint\Editor\Blocks\Image::getImage(
    $block['image-light'], [
        'width' => 350,
        'height' => 350,
        'exact' => 0,
    ]
);
$imageDark = Sprint\Editor\Blocks\Image::getImage(
    $block['image-dark'], [
        'width' => 350,
        'height' => 350,
        'exact' => 0,
    ]
);
$text = Sprint\Editor\Blocks\Text::getValue($block['text']);
?>
<div class="charity__top">
    <div class="circle-top circle-top_left">
        <img class="lazy" data-src="<?= $imageLeft['SRC'] ?>" alt="<?= $imageLeft['DESCRIPTION'] ?>">
    </div>
    <div class="circle-top circle-top_right">
        <img class="lazy" data-src="<?= $imageRight['SRC'] ?>" alt="<?= $imageRight['DESCRIPTION'] ?>">
    </div>
    <div class="logo-reverse logo-top">
        <div class="light">
            <img src="<?= $imageLight['SRC'] ?>" alt="<?= $imageLight['DESCRIPTION'] ?>">
        </div>
        <div class="dark">
            <img src="<?= $imageDark['SRC'] ?>" alt="<?= $imageDark['DESCRIPTION'] ?>">
        </div>
    </div>
    <div class="content__cite cite"><?= $text ?></div>
</div>