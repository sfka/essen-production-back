<? /** @var $block array */ ?><?

$text = Sprint\Editor\Blocks\Text::getValue($block['text']);
$image3 = Sprint\Editor\Blocks\Image::getImage(
    $block['image3'], [
        'width' => 600,
        'height' => 600,
        'exact' => 0,
    ]
);
?>
<div class="appeal">
    <div class="appeal__boss">
        <div class="offset-center">
            <div class="logo-reverse logo-boss">
                <div class="light">
                    <img src="<?= CFile::GetPath(\Bitrix\Main\Config\Option::get( "askaron.settings", "UF_LOGO"));?>" alt="">
                </div>
                <div class="dark">
                    <img src="<?= CFile::GetPath(\Bitrix\Main\Config\Option::get( "askaron.settings", "UF_LOGO_DARK"));?>" alt="">
                </div>
            </div>
            <img class="boss lazy" data-src="<?= $image3['SRC'] ?>" alt="">
        </div>
    </div>
    <div class="appeal__cite">
        <div class="offset-center">
            <div class="quote">
                <figcaption>
                    <blockquote><?=$text ?></blockquote>
                    <p><?= $block['valueCount'] ?></p>
                    <cite><?= $block['valueTitle'] ?></cite>
                </figcaption>
            </div>
        </div>
    </div>
</div>
