<? /** @var $block array */
?>
<div class="item-numeral">
    <div class="info-numeral">
        <div class="column count-numeral">
            <div class="count"><?= $block['valueCount'] ?></div>
            <div class="title"><?= $block['valueTitle'] ?></div>
        </div>
        <? if ($block['valueDesc']): ?>
            <div class="column text-numeral">
                <p><?= $block['valueDesc'] ?></p>
            </div>
        <? endif; ?>
    </div>
    <? if ($block['valueQuote']): ?>
        <div class="quote">
            <figcaption>
                <blockquote><?= $block['valueQuote'] ?></blockquote>
            </figcaption>
        </div>
    <? endif; ?>
</div>
