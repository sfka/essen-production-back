<? /** @var $block array */
?>
<? if ($block['items']): ?>
    <div class="list" <?= (count($block['items']) > 3 ? 'data-type="wide"' : false) ?>>
        <? foreach ($block['items'] as $arItem): ?>
            <div class="list-item">
                <div class="label"><?= $arItem['number'] ?></div>
                <div class="contacts-items">
                    <? if ($arItem['phone']): ?>
                        <div class="contacts-item contacts-item_line" data-size="small">
                            <div class="icon-item">
                                <svg class="icon" width="14" height="14">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#phone"></use>
                                </svg>
                            </div>
                            <div class="info-item">
                                <? foreach ($arItem['phone'] as $arContact): ?>
                                    <? if ($arContact != ''): ?>
                                        <p class="desc-item">
                                            <a href="tel:<?= preg_replace('/[^\d+]+/', '', $arContact); ?>"><?= $arContact ?></a>
                                        </p>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if ($arItem['email']): ?>
                        <div class="contacts-item contacts-item_line" data-size="small">
                            <div class="icon-item">
                                <svg class="icon" width="15" height="12">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#email"></use>
                                </svg>
                            </div>
                            <div class="info-item">
                                <? foreach ($arItem['email'] as $arContact): ?>
                                    <? if ($arContact != ''): ?>
                                        <p class="desc-item">
                                            <a href="mailto:<?= $arContact ?>"><?= $arContact ?></a>
                                        </p>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>
