<? /** @var $block array */ ?><?

$image = Sprint\Editor\Blocks\Image::getImage(
    $block['image'], []
);
?>
<div class="gallery" id="target-period_<?= $block['htag']['anchor'] ?>">
    <div class="gallery-img">
        <img class="lazy" alt="<?= $image['DESCRIPTION'] ?>" data-src="<?= $image['SRC'] ?>">
        <div class="gallery-img__desc">
            <div class="gallery-img__period">
                <div class="inner">
                    <<?= $block['htag']['type'] ?> class="date"><?= $block['htag']['value'] ?>
                    </<?= $block['htag']['type'] ?>>
                    <span class="title"><?= $image['DESCRIPTION'] ?></span>
                </div>
            </div>
        </div>
    </div>
</div>
