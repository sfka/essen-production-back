<? /** @var $block array */ ?><?

$text = Sprint\Editor\Blocks\Text::getValue($block['text']);
$image = Sprint\Editor\Blocks\Image::getImage(
    $block['image'], [
        'width' => 320,
        'height' => 240,
        'exact' => 0,
    ]
);
?>
<div class="content__period offset-small">
    <? if ($image): ?>
        <div class="content__period-img">
            <div class="circle-img">
                <img alt="<?= $image['DESCRIPTION'] ?>" src="<?= $image['SRC'] ?>">
            </div>
        </div>
    <? endif; ?>
    <h3><?= $block['image']['desc'] ?></h3>
    <?= $text ?>
</div>
