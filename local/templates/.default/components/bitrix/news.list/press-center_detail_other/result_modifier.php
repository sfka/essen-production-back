<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

// Получить имя и переводы направлений
foreach ($arResult['ITEMS'] as $key => $arItem) {
    if ($arItem['PROPERTIES']['DIRECTION']['VALUE']) {
        $res = \CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => $arItem['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], 'ID' => $arItem['PROPERTIES']['DIRECTION']['VALUE']],
            false,
            ['nPageSize' => 1],
            ['ID', 'NAME', 'PROPERTY_NAME_EN', 'PROPERTY_NAME_CH']
        )->Fetch();
        $arResult['ITEMS'][$key]['PROPERTIES']['DIRECTION']['DISPLAY_VALUE'] = $res;
    }
}