<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (is_array($arResult["ITEMS"])): ?>
    <div class="news__column news__column_others">
        <div class="news__others">
            <h5><?= GetMessage('OTHER_NEWS') ?>:</h5>
            <div class="news__others-items">
                <? foreach ($arResult["ITEMS"] as $arItem): ?>
                    <? $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']); ?>
                    <a class="news__others-item" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                        <? if ($arItem['PREVIEW_PICTURE']['SRC']): ?>
                            <div class="news__others-img circle-img">
                                <img class="lazy" data-src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $name ?>">
                            </div>
                        <? endif; ?>
                        <h6><?= $name ?></h6>
                    </a>
                <? endforeach; ?>
            </div>
        </div>
    </div>
<? endif; ?>