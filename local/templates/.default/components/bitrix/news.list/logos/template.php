<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="popup-brands">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <? $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']); ?>
        <div class="popup-brands__item">
            <div class="popup-brands__img">
                <img class="lazy" data-src="<?= CFile::GetPath($arItem['PROPERTIES']['FILE_SVG']['VALUE']) ?>" alt="">
            </div>
            <div class="popup-brands__cnt">
                <p><?= GetMessage('LOGO_VERSION_DESC') ?>, <?= $name ?></p>
                <p><span><?= GetMessage('DOWNLOAD') ?>:</span>
                    <? if ($arItem['PROPERTIES']['FILE_EPS']['VALUE']): ?>
                        <a href="<?= CFile::GetPath($arItem['PROPERTIES']['FILE_EPS']['VALUE']) ?>" download=""
                           target="_blank"><?= GetMessage('EPS') ?></a>,
                    <? endif; ?>
                    <? if ($arItem['PROPERTIES']['FILE_PNG']['VALUE']): ?>
                        <a href="<?= CFile::GetPath($arItem['PROPERTIES']['FILE_PNG']['VALUE']) ?>" download=""
                           target="_blank"><?= GetMessage('PNG') ?></a>,
                    <? endif; ?>
                    <? if ($arItem['PROPERTIES']['FILE_SVG']['VALUE']): ?>
                        <a href="<?= CFile::GetPath($arItem['PROPERTIES']['FILE_SVG']['VALUE']) ?>" download=""
                           target="_blank"><?= GetMessage('SVG') ?></a>,
                    <? endif; ?>
                    <? if ($arItem['PROPERTIES']['FILE_PDF']['VALUE']): ?>
                        <a href="<?= CFile::GetPath($arItem['PROPERTIES']['FILE_PDF']['VALUE']) ?>" download=""
                           target="_blank"><?= GetMessage('PDF') ?></a>
                    <? endif; ?>
                </p>
            </div>
        </div>
    <? endforeach; ?>
</div>