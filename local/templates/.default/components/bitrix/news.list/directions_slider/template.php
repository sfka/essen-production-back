<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="page__list scrollable-list swiper-container" data-view="wide" data-filter="not">
    <div class="swiper-wrapper">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);

            $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
            $longName = (SITE_ID == 's1' ? $arItem['PROPERTIES']['LONG_NAME']['VALUE'] : $arItem['PROPERTIES']['LONG_NAME_' . strtoupper(SITE_ID)]['VALUE']);
            $numbersText = (SITE_ID == 's1' ? $arItem['PROPERTIES']['NUMBERS_TEXT']['VALUE'] : $arItem['PROPERTIES']['NUMBERS_TEXT_' . strtoupper(SITE_ID)]['VALUE']);
            $numbersDesc = (SITE_ID == 's1' ? $arItem['PROPERTIES']['NUMBERS_TEXT']['DESCRIPTION'] : $arItem['PROPERTIES']['NUMBERS_TEXT_' . strtoupper(SITE_ID)]['DESCRIPTION']);
            ?>
            <div class="swiper-slide">
                <div class="directions-card card" data-width="wide" data-type="direction">
                    <div class="directions-card__row">
                        <a class="directions-card__left column" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                            <div class="directions-card__img-wrapper">
                                <div class="directions-card__mask"></div>
                                <div class="directions-card__img"
                                     style="background-image: url(<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>)"></div>
                            </div>
                            <div class="directions-card__inner card__inner">
                                <div class="directions-card__text">
                                    <div class="directions-card__title"><?= $name ?></div>
                                    <div class="directions-card__desc"><?= $longName ?></div>
                                </div>
                                <div class="directions-card__btn card__btn">
                                    <svg class="card__btn-icon" width="36" height="24">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                                    </svg>
                                </div>
                            </div>
                        </a>
                        <div class="directions-card__right column">
                            <div class="directions-card__inner card__inner">
                                <div class="inner-right">
                                    <div class="brand-right">
                                        <img class="lazy" data-src="<?= CFile::GetPath($arItem['PROPERTIES']['LOGO_DIRECTION']['VALUE']) ?>"
                                             alt=""/>
                                    </div>
                                    <div class="info-right">
                                        <? if ($arItem['PROPERTIES']['NUMBERS']['VALUE']): ?>
                                            <div class="circle-right">
                                                <div class="inner">
                                                    <div class="circle-right__num"><?= $arItem['PROPERTIES']['NUMBERS']['VALUE'] ?></div>
                                                    <div class="circle-right__title"><?= $arItem['PROPERTIES']['NUMBERS_TEXT']['VALUE'] ?></div>
                                                    <div class="circle-right__text"><?= $arItem['PROPERTIES']['NUMBERS_TEXT']['DESCRIPTION'] ?></div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arItem['PROPERTIES']['LINK']['VALUE']): ?>
                                            <a class="more-link" href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>"
                                               target="_blank"><?= GetMessage('LINK_TO_SITE') ?></a>
                                        <? endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>