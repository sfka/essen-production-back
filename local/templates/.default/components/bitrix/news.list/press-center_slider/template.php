<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($_REQUEST['PAGEN_' . $arResult['NAV_RESULT']->NavNum] <= $arResult['NAV_RESULT']->NavPageCount): ?>
    <div class="page__list scrollable-list ajax__scroll_slider swiper-container"
         data-all-page="<?= $arResult['NAV_RESULT']->NavPageCount ?>"
         data-pagen="<?= $arResult['NAV_RESULT']->NavNum ?>"
         data-page="<?= $arResult['NAV_RESULT']->PAGEN ?>"
         data-script="false"
        <? if ($arParams['FILTER_OPTIONS']) echo 'data-filter=\'' . json_encode($arParams['FILTER_OPTIONS']) . '\''; ?>>
        <div class="swiper-wrapper ajax__scroll_row">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);

                $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
                $direction = (SITE_ID == 's1' ? $arItem['PROPERTIES']['DIRECTION']['DISPLAY_VALUE']['NAME'] : $arItem['PROPERTIES']['DIRECTION']['DISPLAY_VALUE']['PROPERTY_NAME_' . strtoupper(SITE_ID) . '_VALUE'])
                ?>
                <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <a class="news-card card" href="<?= $arItem['DETAIL_PAGE_URL'] ?>" data-type="news">
                        <div class="news-card__img-wrapper">
                            <div class="news-card__mask"></div>
                            <div class="news-card__img"
                                 style="background-image: url(<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>)"></div>
                        </div>
                        <div class="news-card__inner card__inner">
                            <div class="tag tag_light tag_md">
                                <svg class="tag__icon" width="40" height="40">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#tag"></use>
                                </svg>
                                <div class="tag__text"><?= $direction ?></div>
                            </div>
                            <div class="news-card__text">
                                <div class="news-card__title"><?= $name ?></div>
                            </div>
                            <? if ($arItem['DISPLAY_ACTIVE_FROM']): ?>
                                <div class="news-card__date"><?= $arItem['DISPLAY_ACTIVE_FROM'] ?></div>
                            <? endif; ?>
                            <div class="news-card__btn card__btn">
                                <svg class="card__btn-icon" width="36" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                                </svg>
                            </div>
                        </div>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>


    <div class="popup-wrapper" id="popupNews">
        <div class="popup-wrapper__close popup-close magnetic-wrap">
            <svg class="popup-wrapper__close-icon" width="22" height="22">
                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
            </svg>
        </div>
        <div class="popup-wrapper__overlay">
            <div class="page__heading"><?= GetMessage('PRESS_CENTER') ?></div>
            <div class="ajax__update_content">
                <? if ($arParams['AJAX_MODAL'] == 'Y'): ?>
                    <div class="popup-wrapper__list ajax__scroll_modal"
                         data-all-page="<?= $arResult['NAV_RESULT']->NavPageCount ?>"
                         data-pagen="<?= $arResult['NAV_RESULT']->NavNum ?>"
                         data-page="<?= $arResult['NAV_RESULT']->PAGEN ?>"
                         data-script="true"
                        <? ($arParams['FILTER_OPTIONS'] ? 'data-filter="' . json_encode($arParams['FILTER_OPTIONS']) . '"' : false) ?>>

                        <div class="card__row ajax__scroll_row">
                            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                                <?
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);

                                $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
                                $direction = (SITE_ID == 's1' ? $arItem['PROPERTIES']['DIRECTION']['DISPLAY_VALUE']['NAME'] : $arItem['PROPERTIES']['DIRECTION']['DISPLAY_VALUE']['PROPERTY_NAME_' . strtoupper(SITE_ID) . '_VALUE']); ?>
                                <div class="card-col">
                                    <a class="news-card card" href="<?= $arItem['DETAIL_PAGE_URL'] ?>" data-type="news">
                                        <div class="news-card__img-wrapper">
                                            <div class="news-card__mask"></div>
                                            <div class="news-card__img"
                                                 style="background-image: url(<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>)"></div>
                                        </div>
                                        <div class="news-card__inner card__inner">
                                            <div class="tag tag_light tag_md">
                                                <svg class="tag__icon" width="40" height="40">
                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#tag"></use>
                                                </svg>

                                                <div class="tag__text"><?= $direction ?></div>
                                            </div>
                                            <div class="news-card__text">
                                                <div class="news-card__title"><?= $name ?></div>
                                            </div>
                                            <div class="news-card__date"><?= $arItem['DISPLAY_ACTIVE_FROM'] ?></div>
                                            <div class="news-card__btn card__btn">
                                                <svg class="card__btn-icon" width="36" height="24">
                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                                                </svg>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
<? endif; ?>

<? $this->SetViewTarget('footer_add_blocks'); ?>
<div class="page__links">
    <a class="page__link popup-open" href="#popupPressKit">
        <div class="page__link-icon-wrapper">
            <svg class="page__link-icon" width="12" height="16">
                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#document"></use>
            </svg>
        </div>
        <span class="page__link-text" data-text="<?= GetMessage('PRESS_KIT') ?>"><?= GetMessage('PRESS_KIT') ?></span>
    </a>
</div>
<a class="page__btn popup-open ajax__modal_list" href="#popupNews">
    <span class="page__btn-text"
          data-text="<?= GetMessage('PRESS_CENTER_ALL') ?>"><?= GetMessage('PRESS_CENTER_ALL') ?></span>
    <svg class="page__btn-icon" width="13" height="13">
        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#squares"></use>
    </svg>
</a>
<? $this->EndViewTarget(); ?>
