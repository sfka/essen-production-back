<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
    <div class="page__list scrollable-list swiper-container" data-view="wide" data-filter="not">
        <div class="swiper-wrapper">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);

                $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
                $contentProperty = (SITE_ID == 's1' ? 'CONTENT' : 'CONTENT_' . strtoupper(SITE_ID));
                ?>
                <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <div class="info-card card" data-type="contacts">
                        <div class="info-card__inner card__inner">
                            <div class="info-card__contacts">
                                <div class="title"><?= $name ?></div>
                                <? $APPLICATION->IncludeComponent(
                                    "sprint.editor:blocks",
                                    'contacts_block',
                                    [
                                        "ELEMENT_ID" => $arItem["ID"],
                                        "IBLOCK_ID" => $arItem["IBLOCK_ID"],
                                        "PROPERTY_CODE" => "$contentProperty",
                                    ],
                                    $component,
                                    [
                                        "HIDE_ICONS" => "Y",
                                    ]
                                ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? $this->SetViewTarget('footer_add_blocks'); ?>
    <div class="page__links">
        <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/contacts_address.php', [], ['SHOW_BORDER' => false]) ?>
        <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/forms/contacts_form.php', [], ['SHOW_BORDER' => false]) ?>
    </div>
    <div class="page__socials">
        <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/social_links.php', [], ['SHOW_BORDER' => false]) ?>
    </div>
<? $this->EndViewTarget(); ?>