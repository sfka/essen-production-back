<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(is_array($arParams['ADD_SLIDER_FROM_IBLOCK_ELEMENT'])){
    foreach($arParams['ADD_SLIDER_FROM_IBLOCK_ELEMENT'] as $iblock => $elem) {
        $arSelect = ["ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "PROPERTY_*"];
        $arFilter = ["IBLOCK_ID" => $iblock, 'ID' => $elem, 'ACTIVE' => 'Y'];
        $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $arFields['PROPERTIES'] = $arProps;
            array_unshift($arResult['ITEMS'], $arFields);
        }
    }
}