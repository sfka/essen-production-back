<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="slides swiper-container">
    <div class="slides__inner swiper-wrapper">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);

            $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
            $longName = (SITE_ID == 's1' ? $arItem['PROPERTIES']['LONG_NAME']['VALUE'] : $arItem['PROPERTIES']['LONG_NAME_' . strtoupper(SITE_ID)]['VALUE']);
            ?>
            <div class="slides__item swiper-slide" id="<?//= $this->GetEditAreaId($arItem['ID']); ?>">
                <? if ($arItem['PROPERTIES']['VIDEO_BG']['VALUE']): ?>
                    <a class="slides__bg"
                       href="<?= ($arItem['PROPERTIES']['LINK_SITE']['VALUE'] ? SITE_DIR . $arItem['PROPERTIES']['LINK_SITE']['VALUE'] : $arItem['DETAIL_PAGE_URL']) ?>">
                        <div class="bg" data-type="video">
                            <div class="bg__inner">
                                <div class="bg__inner-wrap">
                                    <video class="slides__video" autoplay="true" loop="true" muted="true" pip="false"
                                           poster="<?= CFile::GetPath($arItem['PROPERTIES']['PHOTO_BG']['VALUE']) ?>" playsinline>
                                        <source src="<?= $arItem['PROPERTIES']['VIDEO_BG']['VALUE']['path'] ?>"
                                                type="video/mp4" autoplay="true"
                                                loop="true" muted="true">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </a>
                <? endif; ?>
                <div class="slides__content">
                    <div class="slides__line">
                        <div class="slides__label"><?= $name ?></div>
                        <div class="slides__heading heading heading_level-1"><?= $longName ?></div>
                    </div>
                    <div class="slides__line">
                        <div class="slides__row">
                            <? if ($arItem['PROPERTIES']['NUMBERS']['VALUE']): ?>
                                <div class="counter">
                                    <div class="counter__num"><?= $arItem['PROPERTIES']['NUMBERS']['VALUE'] ?></div>
                                    <div class="counter__title">
                                        <?= (SITE_ID == 's1' ? $arItem['PROPERTIES']['NUMBERS_TEXT']['VALUE'] : $arItem['PROPERTIES']['NUMBERS_TEXT_' . strtoupper(SITE_ID)]['VALUE']) ?>
                                    </div>
                                    <div class="counter__text">
                                        <?= (SITE_ID == 's1' ? $arItem['PROPERTIES']['NUMBERS_TEXT']['DESCRIPTION'] : $arItem['PROPERTIES']['NUMBERS_TEXT_' . strtoupper(SITE_ID)]['DESCRIPTION']) ?>
                                    </div>
                                </div>
                            <? endif; ?>
                            <a class="main-button magnetic-wrap"
                               href="<?= ($arItem['PROPERTIES']['LINK_SITE']['VALUE'] ? SITE_DIR . $arItem['PROPERTIES']['LINK_SITE']['VALUE'] : $arItem['DETAIL_PAGE_URL']) ?>"><span
                                        class="main-button__text main-button__default-text"><?= $name ?></span><span
                                        class="main-button__text main-button__hidden-text"><?= GetMessage('MORE') ?></span></a>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
    <div class="slides__controls slider-controls">
        <div class="slides__prev-btn slider-btn">
            <svg class="slider-btn__icon" width="18" height="12">
                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-left"></use>
            </svg>

        </div>
        <div class="slides__next-btn slider-btn">
            <svg class="slider-btn__icon" width="18" height="12">
                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
            </svg>

        </div>
    </div>
    <div class="slides__scroller">
        <div class="scroller">
            <div class="scroller__text"><?= GetMessage('SCROLL') ?></div>
            <div class="scroller__line">
                <div class="scroller__circle"></div>
            </div>
        </div>
    </div>
</div>
