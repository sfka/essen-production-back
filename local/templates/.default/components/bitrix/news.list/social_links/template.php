<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="socials socials_small socials_line socials_page">
    <div class="socials__label"><?= GetMessage('OUR_SOCIALS') ?>:</div>
    <div class="socials-icons">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]); ?>
            <a class="item-socials <?= $arItem['NAME'] ?>"
               href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>"
               id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
               target="_blank">
                <? if (!empty($arItem['PROPERTIES']['ICO']['VALUE'])): ?>
                    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($arItem['PROPERTIES']['ICO']['VALUE'])) ?>
                <? endif ?>
            </a>
        <? endforeach; ?>
    </div>
</div>