<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($_REQUEST['PAGEN_' . $arResult['NAV_RESULT']->NavNum] <= $arResult['NAV_RESULT']->NavPageCount): ?>
    <div class="page__list scrollable-list ajax__scroll_slider swiper-container"
         data-all-page="<?= $arResult['NAV_RESULT']->NavPageCount ?>"
         data-pagen="<?= $arResult['NAV_RESULT']->NavNum ?>"
         data-page="<?= $arResult['NAV_RESULT']->PAGEN ?>"
         data-script="true">
        <div class="swiper-wrapper ajax__scroll_row">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);

                $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
                $direction = (SITE_ID == 's1' ? $arItem['PROPERTIES']['DIRECTION']['DISPLAY_VALUE']['NAME'] : $arItem['PROPERTIES']['DIRECTION']['DISPLAY_VALUE']['PROPERTY_NAME_' . strtoupper(SITE_ID) . '_VALUE']);
                $city = (SITE_ID == 's1' ? $arItem['PROPERTIES']['CITY']['DISPLAY_VALUE']['NAME'] : $arItem['PROPERTIES']['CITY']['DISPLAY_VALUE']['PROPERTY_NAME_' . strtoupper(SITE_ID) . '_VALUE']);
                $arVacancy = (SITE_ID == 's1' ? $arItem['PROPERTIES']['ABOUT_VACANCY'] : $arItem['PROPERTIES']['ABOUT_VACANCY_' . strtoupper(SITE_ID)]);
                ?>
                <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <a class="info-card card popup-open ajax__vacancy_modal" href="#popupVacanciesForm"
                       data-type="vacancies"
                       data-vacancy="<?= $arItem['ID'] ?>">
                        <div class="info-card__inner card__inner">
                            <? if ($city): ?>
                                <div class="tag tag_blue tag_md">
                                    <svg class="tag__icon" width="40" height="40">
                                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#tag"></use>
                                    </svg>
                                    <div class="tag__text"><?= GetMessage('CITY_SHORT') ?> <?= $city ?></div>
                                </div>
                            <? endif; ?>
                            <div class="info-card__vacancies">
                                <div class="type"><?= $direction ?></div>
                                <div class="title"><?= $name ?></div>
                                <? if ($arVacancy['VALUE']): ?>
                                    <div class="list">
                                        <? foreach ($arVacancy['VALUE'] as $key => $vacancy): ?>
                                            <div class="list-item">
                                                <? if ($arVacancy['DESCRIPTION'][$key]): ?>
                                                    <span class="label"><?= $arVacancy['DESCRIPTION'][$key] ?>:</span>
                                                <? endif; ?>
                                                <div class="desc"><?= $vacancy['TEXT'] ?></div>
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                <? endif; ?>
                            </div>
                            <div class="info-card__btn card__btn">
                                <svg class="card__btn-icon" width="36" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                                </svg>
                            </div>
                        </div>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>


    <div class="popup-wrapper" id="popupVacancies">
        <div class="popup-wrapper__close popup-close magnetic-wrap">
            <svg class="popup-wrapper__close-icon" width="22" height="22">
                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
            </svg>
        </div>
        <div class="popup-wrapper__overlay">
            <div class="page__heading"><?= GetMessage('VACANCIES') ?></div>

            <div class="popup-wrapper__list ajax__update_content ajax__scroll_modal"
                 data-all-page="<?= $arResult['NAV_RESULT']->NavPageCount ?>"
                 data-pagen="<?= $arResult['NAV_RESULT']->NavNum ?>"
                 data-page="<?= $arResult['NAV_RESULT']->PAGEN ?>"
                 data-script="true">
                <? if ($arParams['AJAX_MODAL'] == 'Y'): ?>
                    <div class="card__row ajax__scroll_row">
                        <? foreach ($arResult["ITEMS"] as $arItem): ?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);

                            $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
                            $direction = (SITE_ID == 's1' ? $arItem['PROPERTIES']['DIRECTION']['DISPLAY_VALUE']['NAME'] : $arItem['PROPERTIES']['DIRECTION']['DISPLAY_VALUE']['PROPERTY_NAME_' . strtoupper(SITE_ID) . '_VALUE']);
                            $city = (SITE_ID == 's1' ? $arItem['PROPERTIES']['CITY']['DISPLAY_VALUE']['NAME'] : $arItem['PROPERTIES']['CITY']['DISPLAY_VALUE']['PROPERTY_NAME_' . strtoupper(SITE_ID) . '_VALUE']);
                            $arVacancy = (SITE_ID == 's1' ? $arItem['PROPERTIES']['ABOUT_VACANCY'] : $arItem['PROPERTIES']['ABOUT_VACANCY_' . strtoupper(SITE_ID)]);
                            ?>
                            <div class="card-col">
                                <a class="info-card card popup-open ajax__vacancy_modal" href="#popupVacanciesForm"
                                   data-vacancy="<?= $arItem['ID'] ?>" data-type="vacancies">
                                    <div class="info-card__inner card__inner">
                                        <? if ($city): ?>
                                            <div class="tag tag_blue tag_md">
                                                <svg class="tag__icon" width="40" height="40">
                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#tag"></use>
                                                </svg>
                                                <div class="tag__text"><?= GetMessage('CITY_SHORT') ?> <?= $city ?></div>
                                            </div>
                                        <? endif; ?>
                                        <div class="info-card__vacancies">
                                            <div class="type"><?= $direction ?></div>
                                            <div class="title"><?= $name ?></div>
                                            <? if ($arVacancy['VALUE']): ?>
                                                <div class="list">
                                                    <? foreach ($arVacancy['VALUE'] as $key => $vacancy): ?>
                                                        <div class="list-item"><span
                                                                    class="label"><?= $arVacancy['DESCRIPTION'][$key] ?>:</span>
                                                            <div class="desc"><?= $vacancy['TEXT'] ?></div>
                                                        </div>
                                                    <? endforeach; ?>
                                                </div>
                                            <? endif; ?>
                                        </div>
                                        <div class="info-card__btn card__btn">
                                            <svg class="card__btn-icon" width="36" height="24">
                                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
<? endif; ?>

<? // модалка конкретной вакансии ?>
<div class="popup-wrapper" id="popupVacanciesForm">
    <div class="popup-wrapper__close popup-close magnetic-wrap">
        <svg class="popup-wrapper__close-icon" width="22" height="22">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </div>
    <div class="popup-wrapper__bg">
        <div class="popup-wrapper__bg-left"></div>
        <div class="popup-wrapper__bg-right"></div>
    </div>
    <div class="popup-wrapper__inner ajax__update_content">
        <? if ($arParams['AJAX_MODAL'] == 'Y' && is_array($arParams['VACANCY_DETAIL'])): ?>
            <? $arVacancyDetail = $arParams['VACANCY_DETAIL'];
            $vName = (SITE_ID == 's1' ? $arVacancyDetail['NAME'] : $arVacancyDetail['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
            $vArVacancy = (SITE_ID == 's1' ? $arVacancyDetail['PROPERTIES']['ABOUT_VACANCY_FULL'] : $arVacancyDetail['PROPERTIES']['ABOUT_VACANCY_FULL_' . strtoupper(SITE_ID)]);
            $vDir = \CIBlockElement::GetList(
                [],
                ['IBLOCK_ID' => $arVacancyDetail['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], 'ID' => $arVacancyDetail['PROPERTIES']['DIRECTION']['VALUE']],
                false,
                ['nPageSize' => 1],
                ['ID', 'NAME', 'PROPERTY_NAME_EN', 'PROPERTY_NAME_CH']
            )->Fetch();
            ?>
            <div class="popup-wrapper__left-col">
                <div class="content-container">
                    <? if ($city): ?>
                        <div class="tag tag_blue tag_md">
                            <svg class="tag__icon" width="40" height="40">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#tag"></use>
                            </svg>
                            <div class="tag__text"><?= GetMessage('CITY_SHORT') ?> <?= $city ?></div>
                        </div>
                    <? endif; ?>
                    <h2><?= $vName ?></h2>
                    <? if ($vArVacancy['VALUE']): ?>
                        <div class="offset-top">
                            <? foreach ($vArVacancy['~VALUE'] as $key => $vacancy): ?>
                                <? if ($vArVacancy['DESCRIPTION'][$key]): ?>
                                    <h4><?= $vArVacancy['DESCRIPTION'][$key] ?>:</h4>
                                <? endif; ?>
                                <div><?= $vacancy['TEXT'] ?></div>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>
                </div>
            </div>
            <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/forms/vacancy.php', ['VACANCY' => $arVacancyDetail['ID']], ['SHOW_BORDER' => false]) ?>
        <? endif; ?>
    </div>
</div>

<? $this->SetViewTarget('footer_add_blocks'); ?>
<div class="page__links">
    <a class="page__link" href="<?= \Bitrix\Main\Config\Option::get("askaron.settings", "UF_HH_LINK"); ?>"
       target="_blank">
        <div class="page__link-icon-wrapper">
            <svg class="page__link-icon" width="14" height="10">
                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#hh"></use>
            </svg>
        </div>
        <span class="page__link-text"
              data-text="<?= GetMessage('VACANCIES_HH') ?>"><?= GetMessage('VACANCIES_HH') ?></span>
    </a>
</div>
<a class="page__btn popup-open ajax__modal_list" href="#popupVacancies">
    <span class="page__btn-text"
          data-text="<?= GetMessage('VACANCIES_ALL') ?>"><?= GetMessage('VACANCIES_ALL') ?></span>
    <svg class="page__btn-icon" width="13" height="13">
        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#squares"></use>
    </svg>
</a>
<? $this->EndViewTarget(); ?>
