<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
// Фильтрация по свойствам
/*$arResult['PROPERTY_FILTER'] = [];
foreach ($arParams['FILTER_PROPERTY_CODE'] as $filterCode) {
    $allCount = 0;
    $resProp = CIBlockProperty::GetByID($filterCode, $arParams['IBLOCK_ID']);
    if ($arProp = $resProp->GetNext()) {
        if ($arProp['PROPERTY_TYPE'] == 'E' && $arProp['LINK_IBLOCK_ID']) {
            $res = \CIBlockElement::GetList(
                [],
                ['IBLOCK_ID' => $arProp['LINK_IBLOCK_ID'], 'ACTIVE' => 'Y'],
                false,
                false,
                ['ID', 'NAME', 'PROPERTY_NAME_EN', 'PROPERTY_NAME_CH']
            );
            $arResult['PROPERTY_FILTER']['PROPERTY_' . $arProp['CODE']][0] = ['NAME' => $arProp['HINT'], 'CLEAR' => 'Y'];
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $arResult['PROPERTY_FILTER']['PROPERTY_' . $arProp['CODE']][$arFields['ID']] = $arFields;
                if($arParams['SHOW_FILTER_PROPERTY_COUNT'] == 'Y') {
                    $resCount = \CIBlockElement::GetList(
                        ['cnt'],
                        ['IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'PROPERTY_' . $arProp['CODE'] => $arFields['ID']],
                        [],
                        false,
                        []
                    );
                    if($resCount > 0) {
                        $arResult['PROPERTY_FILTER']['PROPERTY_' . $arProp['CODE']][$arFields['ID']]['COUNT'] = $resCount;
                        $allCount += $resCount;
                    } else {
                        unset($arResult['PROPERTY_FILTER']['PROPERTY_' . $arProp['CODE']][$arFields['ID']]);
                    }
                    unset($resCount);
                    $arResult['PROPERTY_FILTER']['PROPERTY_' . $arProp['CODE']][0]['COUNT'] = $allCount;
                }
            }
        }
        if ($arProp['PROPERTY_TYPE'] == 'L') {

        }
    }
}*/
//\IL\Utilities::DB($arResult['PROPERTY_FILTER']);

// Получить имя и переводы направлений
foreach ($arResult['ITEMS'] as $key => $arItem) {
    if ($arItem['PROPERTIES']['DIRECTION']['VALUE']) {
        $res = \CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => $arItem['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], 'ID' => $arItem['PROPERTIES']['DIRECTION']['VALUE']],
            false,
            ['nPageSize' => 1],
            ['ID', 'NAME', 'PROPERTY_NAME_EN', 'PROPERTY_NAME_CH']
        )->Fetch();
        $arResult['ITEMS'][$key]['PROPERTIES']['DIRECTION']['DISPLAY_VALUE'] = $res;
    }
    if ($arItem['PROPERTIES']['CITY']['VALUE']) {
        $res = \CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => $arItem['PROPERTIES']['CITY']['LINK_IBLOCK_ID'], 'ID' => $arItem['PROPERTIES']['CITY']['VALUE']],
            false,
            ['nPageSize' => 1],
            ['ID', 'NAME', 'PROPERTY_NAME_EN', 'PROPERTY_NAME_CH']
        )->Fetch();
        $arResult['ITEMS'][$key]['PROPERTIES']['CITY']['DISPLAY_VALUE'] = $res;
    }
}