<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest();
$filter = $arParams["FILTER_NAME"];

if ($ajax) {
    $APPLICATION->RestartBuffer();
    global $$filter;
    foreach ($arParams['FILTER_PROPERTY_CODE'] as $filterProp) {
        $$filter['PROPERTY_' . $filterProp] = $request->get('PROPERTY_' . $filterProp);
    }
    if ($request->get('AJAX_MODAL')) {
        $ajaxModal = 'Y';
    }
    if ($request->get('AJAX_MODAL') == 'Y' && $request->get('VACANCY_ID')) {
        $arSelect = ["ID", "IBLOCK_ID", "NAME", "PROPERTY_*"];
        $arFilter = ["IBLOCK_ID" => $arParams['IBLOCK_ID'], 'ID' => $request->get('VACANCY_ID'), 'ACTIVE' => 'Y'];
        $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $arFields['PROPERTIES'] = $arProps;
        }
        $arVacancyDetail = $arFields;
    }
}
/*if (isset($_REQUEST['PROPERTY_DIRECTION'])) {
    global $$filter;
    $$filter = [
        'PROPERTY_DIRECTION' => $_REQUEST['PROPERTY_DIRECTION'],
    ];
}*/
/*global $$filter;
foreach ($_REQUEST as $key => $value) {
    if (substr($key, 0, 9) == 'PROPERTY_') {
        $$filter[$key] = $value;
    }
}
echo'<pre>';
print_r($$filter);
echo'</pre>';*/
?>

<? foreach ($arResult['PROPERTY_FILTER'] as $keyProp => $arProp) {
    if (isset($$filter[$keyProp]) && !empty($$filter[$keyProp])) {
        $currentFilter[$keyProp] = $arResult['PROPERTY_FILTER'][$keyProp][$$filter[$keyProp]];
    } else {
        $currentFilter[$keyProp] = $arResult['PROPERTY_FILTER'][$keyProp][0];
    }
} ?>
    <div class="ajax__update_content page__list">
        <? if ($arParams["USE_FILTER"] == "Y"): ?>
            <div class="page__top-row">
                <div class="page__label"><?= GetMessage('FILTER') ?>:</div>
                <div class="page__filters js__filter" data-filter='<?= json_encode($$filter) ?>'>
                    <? foreach ($currentFilter as $keyProp => $arProp): ?>
                        <a class="page__filter filter-open" href="#<?= strtolower($keyProp) ?>">
                            <div class="page__filter-label"
                                 data-text="<?= $arProp['NAME'] ?> <?= ($arParams['SHOW_FILTER_PROPERTY_COUNT'] == 'Y' ? '(' . $arProp['COUNT'] . ')' : false); ?>"><?= $arProp['NAME'] ?> <?= ($arParams['SHOW_FILTER_PROPERTY_COUNT'] == 'Y' ? '(' . $arProp['COUNT'] . ')' : false); ?></div>
                            <svg class="page__filter-icon" width="9" height="7">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#triangle"></use>
                            </svg>
                        </a>
                    <? endforeach; ?>
                </div>
            </div>
        <? endif ?>
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            $arParams['LIST_TEMPLATE'],
            [
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "NEWS_COUNT" => $arParams["NEWS_COUNT"],
                "SORT_BY1" => $arParams["SORT_BY1"],
                "SORT_ORDER1" => $arParams["SORT_ORDER1"],
                "SORT_BY2" => $arParams["SORT_BY2"],
                "SORT_ORDER2" => $arParams["SORT_ORDER2"],
                "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
                "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
                "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                "SET_TITLE" => $arParams["SET_TITLE"],
                "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                "MESSAGE_404" => $arParams["MESSAGE_404"],
                "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                "SHOW_404" => $arParams["SHOW_404"],
                "FILE_404" => $arParams["FILE_404"],
                "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
                "CHECK_DATES" => $arParams["CHECK_DATES"],

                //мои доп свойства
                "FILTER_PROPERTY_CODE" => $arParams['FILTER_PROPERTY_CODE'],
                "SHOW_FILTER_PROPERTY_COUNT" => $arParams['SHOW_FILTER_PROPERTY_COUNT'],
                "USE_FILTER" => $arParams['USE_FILTER'],
                "FILTER_OPTIONS" => $$filter,
                "AJAX_MODAL" => $ajaxModal,
                "VACANCY_DETAIL" => (is_array($arVacancyDetail) ? $arVacancyDetail : false),
            ],
            $component
        );
        unset($$filter);
        if ($ajax) die();
        ?>
    </div>

<? if ($arParams["USE_FILTER"] == "Y"): ?>
    <? foreach ($arResult['PROPERTY_FILTER'] as $key => $propFilter): ?>
        <div class="filter" id="<?= strtolower($key) ?>">
            <div class="filter__close filter-close magnetic-wrap">
                <svg class="filter__close-icon" width="22" height="22">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
                </svg>
            </div>
            <div class="filter__overlay">
                <div class="filter__inner">
                    <div class="filter__list loop-list swiper-container">
                        <div class="filter__list-inner loop-list__inner swiper-wrapper">
                        <? foreach ($propFilter as $arProp): ?>
                                <div class="filter__list-item loop-list__item swiper-slide">
                                    <label class="filter__list-link loop-list__link ajax__filter_elements"
                                           data-prop="<?= str_replace('PROPERTY_', '', $key); ?>"
                                           data-value="<?= $arProp['ID'] ?>">
                                        <div class="title-link" data-hover="<?= $arProp['NAME'] ?>"><?= $arProp['NAME'] ?>
                                            <? if ($arParams['SHOW_FILTER_PROPERTY_COUNT'] == 'Y'): ?>
                                                <span class="count-link"><?= $arProp['COUNT'] ?></span>
                                            <? endif; ?>
                                        </div>
                                    </label>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <? endforeach; ?>
<? endif; ?>