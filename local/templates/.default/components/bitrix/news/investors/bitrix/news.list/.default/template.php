<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="page__list scrollable-list swiper-container" data-view="wide" data-filter="not">
    <div class="swiper-wrapper">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), ["CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);

            $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
            $longName = (SITE_ID == 's1' ? $arItem['PROPERTIES']['LONG_NAME']['VALUE'] : $arItem['PROPERTIES']['LONG_NAME_' . strtoupper(SITE_ID)]['VALUE']);
            ?>
            <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <a class="info-card card" href="<?= $arItem['DETAIL_PAGE_URL'] ?>" data-type="investors">
                    <div class="info-card__inner card__inner">
                        <div class="info-card__investors">
                            <div class="title"><?= $name ?></div>
                            <div class="desc"><?= $longName ?></div>
                        </div>
                        <div class="info-card__btn card__btn">
                            <svg class="card__btn-icon" width="36" height="24">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                            </svg>
                        </div>
                    </div>
                </a>
            </div>
        <? endforeach; ?>
    </div>
</div>