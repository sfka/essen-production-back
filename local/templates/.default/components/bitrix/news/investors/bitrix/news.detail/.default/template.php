<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$name = (SITE_ID == 's1' ? $arResult['NAME'] : $arResult['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
$longName = (SITE_ID == 's1' ? $arResult['PROPERTIES']['LONG_NAME']['VALUE'] : $arResult['PROPERTIES']['LONG_NAME_' . strtoupper(SITE_ID)]['VALUE']);

$contentProperty = (SITE_ID == 's1' ? 'CONTENT' : 'CONTENT_' . strtoupper(SITE_ID));
?>
<div class="offset-center">
    <h1><?= $name ?></h1>
    <p><?= $longName ?></p>
    <? $APPLICATION->IncludeComponent(
        "sprint.editor:blocks",
        'investors_detail',
        [
            "ELEMENT_ID" => $arResult["ID"],
            "IBLOCK_ID" => $arResult["IBLOCK_ID"],
            "PROPERTY_CODE" => $contentProperty,
        ],
        $component,
        [
            "HIDE_ICONS" => "Y",
        ]
    ); ?>
</div>