<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if ($arParams['SHOW_CONTACTS'] == 'Y' && $arResult['PROPERTIES']['CONTACTS']['VALUE']) {
    $arContacts = $arResult['PROPERTIES']['CONTACTS'];

    $arSelect = ["ID", "IBLOCK_ID", "NAME", "PROPERTY_*"];
    $arFilter = ["IBLOCK_ID" => $arContacts['LINK_IBLOCK_ID'], 'ID' => $arContacts['VALUE']];
    $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arResult['CONTACT_INFO'] = $arFields;
        $arProps = $ob->GetProperties();
        $arResult['CONTACT_INFO']['PROPERTIES'] = $arProps;
    }
}
if ($arParams['ABOUT_PAGE'] == 'Y') {
    $arSelect = ["ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "PROPERTY_*"];
    $arFilter = ["IBLOCK_ID" => IBLOCK_DIRECTIONS, 'ACTIVE' => 'Y'];
    $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arResult['DIRECTIONS'][$arFields['ID']] = $arFields;
        $arProps = $ob->GetProperties();
        $arResult['DIRECTIONS'][$arFields['ID']]['PROPERTIES'] = $arProps;
    }
}
if($arParams['LEADERSHIP_SLIDER'] == 'Y') {
    $arSelect = ["ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "PROPERTY_*"];
    $arFilter = ["IBLOCK_ID" => IBLOCK_LEADERSHIP, 'ACTIVE' => 'Y'];
    $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arResult['LEADERSHIP'][$arFields['ID']] = $arFields;
        $arProps = $ob->GetProperties();
        $arResult['LEADERSHIP'][$arFields['ID']]['PROPERTIES'] = $arProps;
    }
}