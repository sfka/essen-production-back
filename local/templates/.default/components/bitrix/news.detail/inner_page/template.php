<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$name = (SITE_ID == 's1' ? $arResult['NAME'] : $arResult['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
$longName = (SITE_ID == 's1' ? $arResult['PROPERTIES']['LONG_NAME']['VALUE'] : $arResult['PROPERTIES']['LONG_NAME_' . strtoupper(SITE_ID)]['VALUE']);

$contentProperty = (SITE_ID == 's1' ? 'CONTENT' : 'CONTENT_' . strtoupper(SITE_ID));

//ShowViewContent
$this->SetViewTarget('VIDEO_BG'); ?>
<? if ($arResult['PROPERTIES']['VIDEO_BG']['VALUE']['path']): ?>
    <div class="bg" data-type="video">
        <div class="bg__inner">
            <div class="bg__inner-wrap">
                <video class="slides__video" autoplay="true" loop="true" muted="true" pip="false"
                       poster="<?= CFile::GetPath($arResult['PROPERTIES']['PHOTO_BG']['VALUE']) ?>" playsinline>
                    <source src="<?= $arResult['PROPERTIES']['VIDEO_BG']['VALUE']['path'] ?>"
                            type="video/mp4"
                            loop="true" muted="true">
                </video>
            </div>
        </div>
    </div>
<? elseif ($arResult['PROPERTIES']['PHOTO_BG']['VALUE']): ?>
    <div class="bg" data-type="img">
        <div class="bg__inner">
            <div class="bg__inner-wrap">
                <div class="img"
                     style="background-image: url(<?= CFile::GetPath($arResult['PROPERTIES']['PHOTO_BG']['VALUE']) ?>);"></div>
            </div>
        </div>
    </div>
<? endif; ?>
<? $this->EndViewTarget();
$this->SetViewTarget('LONG_NAME'); ?>
    <div class="slides__label"><?= $name ?></div>
    <div class="slides__heading heading heading_level-1"><?= $longName ?></div>
<? $this->EndViewTarget();
$this->SetViewTarget('NUMBERS'); ?>
<? if ($arParams['SHOW_NUMBERS']): ?>
    <div class="counter counter_active">
        <div class="counter__num"><?= $arResult['PROPERTIES']['NUMBERS']['VALUE'] ?></div>
        <div class="counter__title"><?= (SITE_ID == 's1' ? $arResult['PROPERTIES']['NUMBERS_TEXT']['VALUE'] : $arResult['PROPERTIES']['NUMBERS_TEXT_' . strtoupper(SITE_ID)]['VALUE']) ?></div>
        <div class="counter__text"><?= (SITE_ID == 's1' ? $arResult['PROPERTIES']['NUMBERS_TEXT']['DESCRIPTION'] : $arResult['PROPERTIES']['NUMBERS_TEXT_' . strtoupper(SITE_ID)]['DESCRIPTION']) ?></div>
    </div>
<? endif; ?>
<? $this->EndViewTarget(); ?>
<? $previewText = (SITE_ID == 's1' ? $arResult['PREVIEW_TEXT'] : $arResult['PROPERTIES']['PREVIEW_TEXT_' . strtoupper(SITE_ID)]['~VALUE']['TEXT']) ?>
<? if ($arParams['ABOUT_PAGE'] == 'Y'): ?>
    <? if ($arParams['ABOUT_PAGE_LOGO'] == 'Y'): ?>
        <div class="content__logo content__logo_width-3">
            <div class="logo-reverse logo-top">
                <div class="light">
                    <img src="<?= CFile::GetPath(\Bitrix\Main\Config\Option::get("askaron.settings", "UF_LOGO")) ?>"
                         alt="">
                </div>
                <div class="dark">
                    <img src="<?= CFile::GetPath(\Bitrix\Main\Config\Option::get("askaron.settings", "UF_LOGO_DARK")) ?>"
                         alt="">
                </div>
            </div>
        </div>
    <? endif; ?>
<? endif; ?>
<? if ($arParams['LEADER_PAGE'] == 'Y'): ?>
    <div class="content__cite cite">
        <p>
            <i><?= $previewText ?></i>
        </p>
    </div>
<? endif; ?>
<? if (is_array($arResult['LEADERSHIP'])): ?>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/leadership.php', ['LEADERSHIP' => $arResult['LEADERSHIP']], ['SHOW_BORDER' => false]) ?>
<? endif; ?>
<? if (is_array($arResult['DIRECTIONS']) && $arParams['DIRECTIONS'] == 'Y'): ?>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/directions.php', ['DIRECTIONS' => $arResult['DIRECTIONS'], 'PREVIEW_TEXT' => $previewText], ['SHOW_BORDER' => false]) ?>
<? endif; ?>
<? if ($arParams['EDITOR_TEMPLATE']): ?>
    <? $APPLICATION->IncludeComponent(
        "sprint.editor:blocks",
        $arParams['EDITOR_TEMPLATE'],
        [
            "ELEMENT_ID" => $arResult["ID"],
            "IBLOCK_ID" => $arResult["IBLOCK_ID"],
            "PROPERTY_CODE" => $contentProperty,
        ],
        $component,
        [
            "HIDE_ICONS" => "Y",
        ]
    ); ?>
<? endif; ?>
<? if ($arResult['CONTACT_INFO']): ?>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/contact_info.php', ['CONTACT_INFO' => $arResult['CONTACT_INFO']], ['SHOW_BORDER' => false]) ?>
<? endif; ?>