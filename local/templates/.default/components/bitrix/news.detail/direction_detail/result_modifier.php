<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if ($arResult['PROPERTIES']['CONTACTS']['VALUE']) {
    foreach ($arResult['PROPERTIES']['CONTACTS']['VALUE'] as $val) {
        $arSelect = ["ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_*"];
        $arFilter = ["IBLOCK_ID" => $arResult['PROPERTIES']['CONTACTS']['LINK_IBLOCK_ID'], 'ID' => $val, 'ACTIVE' => 'Y'];
        $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arResult['CONTACTS'][$arFields['ID']] = $arFields;
            $arProps = $ob->GetProperties();
            $arResult['CONTACTS'][$arFields['ID']]['PROPERTIES'] = $arProps;
        }
    }
}