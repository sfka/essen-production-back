<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$name = (SITE_ID == 's1' ? $arResult['NAME'] : $arResult['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
$longName = (SITE_ID == 's1' ? $arResult['PROPERTIES']['LONG_NAME']['VALUE'] : $arResult['PROPERTIES']['LONG_NAME_' . strtoupper(SITE_ID)]['VALUE']);
$directioText = (SITE_ID == 's1' ? $arResult['PROPERTIES']['DIRECTION_DECRIPTION']['~VALUE']['TEXT'] : $arResult['PROPERTIES']['DIRECTION_DECRIPTION_' . strtoupper(SITE_ID)]['~VALUE']['TEXT']);
$directioDesc = (SITE_ID == 's1' ? $arResult['PROPERTIES']['DIRECTION_DECRIPTION']['DESCRIPTION'] : $arResult['PROPERTIES']['DIRECTION_DECRIPTION_' . strtoupper(SITE_ID)]['DESCRIPTION']);
$numbersText = (SITE_ID == 's1' ? $arResult['PROPERTIES']['NUMBERS_TEXT']['VALUE'] : $arResult['PROPERTIES']['NUMBERS_TEXT_' . strtoupper(SITE_ID)]['VALUE']);
$numbersDesc = (SITE_ID == 's1' ? $arResult['PROPERTIES']['NUMBERS_TEXT']['DESCRIPTION'] : $arResult['PROPERTIES']['NUMBERS_TEXT_' . strtoupper(SITE_ID)]['DESCRIPTION']);
$contentProperty = (SITE_ID == 's1' ? 'CONTENT' : 'CONTENT_' . strtoupper(SITE_ID));


//ShowViewContent
$this->SetViewTarget('VIDEO_BG'); ?>
<? if ($arResult['PROPERTIES']['VIDEO_BG']['VALUE']['path']): ?>
    <div class="bg" data-type="video">
        <div class="bg__inner">
            <div class="bg__inner-wrap">
                <video class="slides__video" autoplay="true" loop="true" muted="true" pip="false"
                       poster="<?= CFile::GetPath($arResult['PROPERTIES']['PHOTO_BG']['VALUE']) ?>" playsinline>
                    <source src="<?= $arResult['PROPERTIES']['VIDEO_BG']['VALUE']['path'] ?>"
                            type="video/mp4"
                            loop="true" muted="true">
                </video>
            </div>
        </div>
    </div>
<? elseif ($arResult['PROPERTIES']['PHOTO_BG']['VALUE']): ?>
    <div class="bg" data-type="img">
        <div class="bg__inner">
            <div class="bg__inner-wrap">
                <div class="img"
                     style="background-image: url(<?= CFile::GetPath($arResult['PROPERTIES']['PHOTO_BG']['VALUE']) ?>);"></div>
            </div>
        </div>
    </div>
<? endif; ?>
<? $this->EndViewTarget();
$this->SetViewTarget('LONG_NAME'); ?>
    <div class="slides__label"><?= $name ?></div>
    <div class="slides__heading heading heading_level-1"><?= $longName ?></div>
<? $this->EndViewTarget();
$this->SetViewTarget('NUMBERS'); ?>
    <div class="counter counter_active">
        <div class="counter__num"><?= $arResult['PROPERTIES']['NUMBERS']['VALUE'] ?></div>
        <div class="counter__title"><?= $numbersText ?></div>
        <div class="counter__text"><?= $numbersDesc ?></div>
    </div>
<? $this->EndViewTarget();
?>
    <div class="content__circle">
        <? if ($arResult['PROPERTIES']['LEFT_IMAGE']['VALUE']): ?>
            <div class="content__circle-item content__circle-item_left">
                <img src="<?= CFile::GetPath($arResult['PROPERTIES']['LEFT_IMAGE']['VALUE']) ?>">
            </div>
        <? endif; ?>
        <? if ($arResult['PROPERTIES']['RIGHT_IMAGE']['VALUE']): ?>
            <div class="content__circle-item content__circle-item_right">
                <img src="<?= CFile::GetPath($arResult['PROPERTIES']['RIGHT_IMAGE']['VALUE']) ?>">
            </div>
        <? endif; ?>
    </div>
    <div class="content__top">
        <? if ($arResult['PROPERTIES']['LOGO_DIRECTION']['VALUE']): ?>
            <div class="content__logo content__logo_width-1">
                <div class="logo-reverse logo-top">
                    <div class="light">
                        <img src="<?= CFile::GetPath($arResult['PROPERTIES']['LOGO_DIRECTION']['VALUE']) ?>">
                    </div>
                    <div class="dark">
                        <img src="<?= CFile::GetPath($arResult['PROPERTIES']['LOGO_DIRECTION']['VALUE']) ?>">
                    </div>
                </div>
            </div>
        <? endif; ?>
        <div class="content__cite cite">
            <p><i><?= $directioText ?></i></p>
            <p><i><?= $directioDesc ?></i></p>
        </div>
        <div class="content__main-button">
            <a class="main-button main-button_site magnetic-wrap"
               href="<?= $arResult['PROPERTIES']['LINK']['VALUE'] ?>" target="_blank">
                <span class="main-button__text"><?= GetMessage('LINK_TO_SITE') ?></span>
            </a>
        </div>
    </div>
<? $APPLICATION->IncludeComponent(
    "sprint.editor:blocks",
    'direction_detail',
    [
        "ELEMENT_ID" => $arResult["ID"],
        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
        "PROPERTY_CODE" => $contentProperty,
    ],
    $component,
    [
        "HIDE_ICONS" => "Y",
    ]
); ?>
<? if (is_array($arResult['CONTACTS'])): ?>
    <div class="contacts offset-big">
        <div class="contacts__top">
            <h2><?= GetMessage('CONTACT_INFO_DIRECTION') ?></h2>
            <p><i><?= $name ?></i></p>
        </div>
        <? foreach ($arResult['CONTACTS'] as $arContact) : ?>
            <?
            $contName = (SITE_ID == 's1' ? $arContact['NAME'] : $arContact['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
            $contDep = (SITE_ID == 's1' ? $arContact['PROPERTIES']['DEPARTMENT']['VALUE'] : $arContact['PROPERTIES']['DEPARTMENT_' . strtoupper(SITE_ID)]['VALUE']);
            ?>
            <div class="contacts__row">
                <div class="contacts__column">
                    <figure class="quote">
                        <div class="quote__img circle-img">
                            <img class="lazy" data-src="<?= CFile::GetPath($arContact['PREVIEW_PICTURE']) ?>"
                                 alt="<?= $contName ?>">
                        </div>
                        <figcaption>
                            <p><?= $contName ?></p>
                            <cite><?= $contDep ?></cite>
                        </figcaption>
                    </figure>
                </div>
                <div class="contacts__column">
                    <div class="contacts-items">
                        <div class="contacts-item phone contacts-item_line" data-size="big">
                            <div class="icon-item">
                                <svg class="icon" width="28" height="28">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#phone"></use>
                                </svg>
                            </div>
                            <div class="info-item">
                                <span class="label-item"><?= GetMessage('PHONE') ?>:</span>
                                <p class="desc-item">
                                    <a href="tel:<?= preg_replace('/[^\d+]+/', '', $arContact['PROPERTIES']['PHONE']['VALUE']); ?>"><?= $arContact['PROPERTIES']['PHONE']['VALUE'] ?></a>
                                </p>
                            </div>
                        </div>
                        <div class="contacts-item email contacts-item_line"
                             data-size="big">
                            <div class="icon-item">
                                <svg class="icon" width="29" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#email"></use>
                                </svg>
                            </div>
                            <div class="info-item">
                                <span class="label-item"><?= GetMessage('EMAIL') ?>:</span>
                                <p class="desc-item">
                                    <a href="mailto:<?= $arContact['PROPERTIES']['EMAIL']['VALUE'] ?>"><?= $arContact['PROPERTIES']['EMAIL']['VALUE'] ?></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
        <? if ($arResult['PROPERTIES']['LINK']['VALUE']): ?>
            <div class="contacts__link-button">
                <div class="inner-button">
                    <a class="main-button main-button_site magnetic-wrap"
                       href="<?= $arResult['PROPERTIES']['LINK']['VALUE'] ?>"
                       target="_blank">
                        <span class="main-button__text"><?= GetMessage('LINK_TO_SITE') ?></span>
                    </a>
                </div>
            </div>
        <? endif; ?>
    </div>
<? endif; ?>