<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if ($arParams['SHOW_CONTACTS'] == 'Y' && $arResult['PROPERTIES']['CONTACTS']['VALUE']) {
    $arContacts = $arResult['PROPERTIES']['CONTACTS'];

    $arSelect = ["ID", "IBLOCK_ID", "NAME", "PROPERTY_*"];
    $arFilter = ["IBLOCK_ID" => $arContacts['LINK_IBLOCK_ID'], 'ID' => $arContacts['VALUE']];
    $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arResult['CONTACT_INFO'] = $arFields;
        $arProps = $ob->GetProperties();
        $arResult['CONTACT_INFO']['PROPERTIES'] = $arProps;
    }
}