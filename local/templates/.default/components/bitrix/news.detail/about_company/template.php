<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$name = (SITE_ID == 's1' ? $arResult['NAME'] : $arResult['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
$longName = (SITE_ID == 's1' ? $arResult['PROPERTIES']['LONG_NAME']['VALUE'] : $arResult['PROPERTIES']['LONG_NAME_' . strtoupper(SITE_ID)]['VALUE']);

$contentProperty = (SITE_ID == 's1' ? 'CONTENT' : 'CONTENT_' . strtoupper(SITE_ID));
?>
<div class="slides slides_inner-active swiper-container">
    <div class="slides__inner swiper-wrapper">
        <div class="slides__item swiper-slide">
            <div class="slides__bg slides__bg_active">
                <div class="bg" data-type="video">
                    <div class="bg__inner">
                        <div class="bg__inner-wrap">
                            <video class="slides__video" autoplay="true" loop="true" muted="true" pip="false"
                                   poster="<?= CFile::GetPath($arResult['PROPERTIES']['PHOTO_BG']['VALUE']) ?>" playsinline>
                                <source src="<?= $arResult['PROPERTIES']['VIDEO_BG']['VALUE']['path'] ?>"
                                        type="video/mp4" loop="true" muted="true">
                            </video>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slides__content">
                <div class="slides__line">
                    <div class="slides__label"><?= $name ?></div>
                    <div class="slides__heading heading heading_level-1"><?= $longName ?></div>
                </div>
                <div class="slides__line">
                    <div class="slides__row">
                        <a class="main-button main-button_active magnetic-wrap" href="javascript:void(0);">
                            <span class="main-button__text main-button__default-text"><?= $name ?></span>
                            <span class="main-button__text main-button__hidden-text"><?= GetMessage('MORE') ?></span>
                        </a>
                    </div>
                </div>
                <div class="slides__inner-content scroll-content">
                    <div class="content-inner">
                        <div class="offset-center">
                            <div class="content-title">
                                <h3><?= $name ?></h3>
                            </div>
                            <div class="history">
                                <? $APPLICATION->IncludeComponent(
                                    "sprint.editor:blocks",
                                    $arParams['EDITOR_TEMPLATE'],
                                    [
                                        "ELEMENT_ID" => $arResult["ID"],
                                        "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                                        "PROPERTY_CODE" => "$contentProperty",
                                    ],
                                    $component,
                                    [
                                        "HIDE_ICONS" => "Y",
                                    ]
                                ); ?>
                                <? if ($arResult['CONTACT_INFO']): ?>
                                    <div class="contacts offset-big">
                                        <div class="contacts__top">
                                            <h2><?= GetMessage('CONTACT_INFO') ?></h2>
                                            <p>
                                                <i><?= (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['NAME'] : $arResult['CONTACT_INFO']['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]) ?></i>
                                            </p>
                                        </div>
                                        <div class="contacts__row">
                                            <div class="contacts__column">
                                                <? if ($arResult['CONTACT_INFO']['PROPERTIES']['LOGOS']['VALUE']): ?>
                                                    <div class="contacts__brands">
                                                        <? foreach ($arResult['CONTACT_INFO']['PROPERTIES']['LOGOS']['VALUE'] as $key => $logo): ?>
                                                            <div class="item-brand">
                                                                <img class="lazy" data-src="<?= CFile::GetPath($logo) ?>"
                                                                     alt="<?= $arResult['CONTACT_INFO']['PROPERTIES']['LOGOS']['DESCRIPTION'][$key] ?>">
                                                            </div>
                                                        <? endforeach; ?>
                                                    </div>
                                                <? endif; ?>
                                                <? $previewProp = (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['PROPERTIES']['PREVIEW'] : $arResult['CONTACT_INFO']['PROPERTIES']['PREVIEW_' . strtoupper(SITE_ID)]) ?>
                                                <? if ($previewProp['~VALUE']['TEXT']): ?>
                                                    <div class="contacts__text">
                                                        <?= $previewProp['~VALUE']['TEXT']; ?>
                                                    </div>
                                                <? endif; ?>
                                                <? $requisitesProp = (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['PROPERTIES']['REQUISITES'] : $arResult['CONTACT_INFO']['PROPERTIES']['REQUISITES_' . strtoupper(SITE_ID)]) ?>
                                                <? if ($requisitesProp['VALUE']): ?>
                                                    <div class="contacts__data">
                                                        <? foreach ($requisitesProp['VALUE'] as $key => $val): ?>
                                                            <p><?= $val ?> <?= $requisitesProp['DESCRIPTION'][$key] ?></p>
                                                        <? endforeach; ?>
                                                    </div>
                                                <? endif; ?>
                                            </div>
                                            <div class="contacts__column">
                                                <div class="contacts-items">
                                                    <? $phone = (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['PROPERTIES']['PHONE'] : $arResult['CONTACT_INFO']['PROPERTIES']['PHONE_' . strtoupper(SITE_ID)]) ?>
                                                    <? if ($phone['VALUE']): ?>
                                                        <div class="contacts-item phone" data-size="big">
                                                            <div class="icon-item">
                                                                <svg class="icon" width="28" height="28">
                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#phone"></use>
                                                                </svg>
                                                            </div>
                                                            <div class="info-item"><span
                                                                        class="label-item"><?= GetMessage('PHONE') ?>:</span>
                                                                <p class="desc-item">
                                                                    <a href="tel:<?= preg_replace('/[^\d+]+/', '', $phone['VALUE']); ?>"><?= $phone['VALUE'] ?></a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    <? endif; ?>
                                                    <? $email = (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['PROPERTIES']['EMAIL'] : $arResult['CONTACT_INFO']['PROPERTIES']['EMAIL_' . strtoupper(SITE_ID)]) ?>
                                                    <? if ($email['VALUE']): ?>
                                                        <div class="contacts-item email" data-size="big">
                                                            <div class="icon-item">
                                                                <svg class="icon" width="29" height="24">
                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#email"></use>
                                                                </svg>

                                                            </div>
                                                            <div class="info-item">
                                                                <span class="label-item"><?= GetMessage('EMAIL') ?>:</span>
                                                                <p class="desc-item">
                                                                    <a href="mailto:<?= $email['VALUE'] ?>"><?= $email['VALUE'] ?></a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    <? endif; ?>
                                                    <? $address = (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['PROPERTIES']['ADDRESS'] : $arResult['CONTACT_INFO']['PROPERTIES']['ADDRESS_' . strtoupper(SITE_ID)]) ?>
                                                    <? if ($address['VALUE']): ?>
                                                        <div class="contacts-item address" data-size="big">
                                                            <div class="icon-item">
                                                                <svg class="icon" width="22" height="31">
                                                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#address"></use>
                                                                </svg>
                                                            </div>
                                                            <div class="info-item">
                                                                <span class="label-item"><?= GetMessage('ADDRESS') ?>:</span>
                                                                <p class="desc-item"><?= $address['VALUE'] ?></p>
                                                            </div>
                                                        </div>
                                                    <? endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                            </div>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "bottom_menu",
                                [
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "CHILD_MENU_TYPE" => "left",
                                    "DELAY" => "N",
                                    "MAX_LEVEL" => "1",
                                    "MENU_CACHE_GET_VARS" => [
                                    ],
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "ROOT_MENU_TYPE" => "bottom",
                                    "USE_EXT" => "N",
                                    "COMPONENT_TEMPLATE" => "bottom_menu",
                                ],
                                false
                            ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slides__controls slider-controls">
        <div class="slides__prev-btn slider-btn">
            <svg class="slider-btn__icon" width="18" height="12">
                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-left"></use>
            </svg>
        </div>
        <div class="slides__next-btn slider-btn">
            <svg class="slider-btn__icon" width="18" height="12">
                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
            </svg>
        </div>
    </div>
</div>
