<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$name = (SITE_ID == 's1' ? $arResult['NAME'] : $arResult['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
$address = (SITE_ID == 's1' ? $arResult['PROPERTIES']['ADDRESS']['VALUE'] : $arResult['PROPERTIES']['ADDRESS_' . strtoupper(SITE_ID)]['VALUE']);
$productionAddress = (SITE_ID == 's1' ? $arResult['PROPERTIES']['PRODUCTION_ADDRESSES'] : $arResult['PROPERTIES']['PRODUCTION_ADDRESSES_' . strtoupper(SITE_ID)]); ?>

<div class="popup-wrapper" id="popupContactsAddress">
    <div class="popup-wrapper__close popup-close magnetic-wrap">
        <svg class="popup-wrapper__close-icon" width="22" height="22">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </div>
    <div class="popup-wrapper__bg">
        <div class="popup-wrapper__bg-left"></div>
        <div class="popup-wrapper__bg-right"></div>
    </div>
    <div class="popup-wrapper__inner">
        <div class="popup-wrapper__left-col">
            <div class="content-container">
                <div class="popup-brand__wrapper">
                    <div class="popup-brand popup-brand_small">
                        <img src="<?= CFile::GetPath(\Bitrix\Main\Config\Option::get( "askaron.settings", "UF_LOGO"));?>" alt="">
                    </div>
                </div>
                <div class="popup-contacts__address">
                    <div class="label label_xs">
                        <div class="label__icon">
                            <svg class="icon" width="15" height="21">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#map"></use>
                            </svg>

                        </div>
                        <div class="label__text"><?= $name ?></div>
                    </div>
                    <h2><?= GetMessage('LEGAL_ADDRESS') ?></h2>
                    <p><?= $address ?></p>
                </div>
            </div>
        </div>
        <div class="popup-wrapper__right-col">
            <div class="content-container">
                <h2><?= GetMessage('PRODUCTION_ADDRESSES') ?></h2>
                <div class="popup-contacts__addresses">
                    <? foreach ($productionAddress['VALUE'] as $key => $value): ?>
                        <div class="popup-contacts__addresses-item">
                            <h4><?= $value ?></h4>
                            <p><?= $productionAddress['DESCRIPTION'][$key] ?></p>
                        </div>
                    <? endforeach; ?>
                </div>
                <div class="popup-socials">
                    <div class="socials socials_big">
                        <? $APPLICATION->IncludeComponent("bitrix:news.list",
                            "social_links", [
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "N",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "A",
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "N",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "FIELD_CODE" => [
                                    0 => "",
                                ],
                                "FILTER_NAME" => "",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "IBLOCK_ID" => "17",
                                "IBLOCK_TYPE" => "widgets",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "INCLUDE_SUBSECTIONS" => "N",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "20",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Новости",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "PROPERTY_CODE" => [
                                    0 => "LINK",
                                    1 => "ICO",
                                ],
                                "SET_BROWSER_TITLE" => "N",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "SORT",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER1" => "ASC",
                                "SORT_ORDER2" => "ASC",
                                "STRICT_SECTION_CHECK" => "N",
                            ],
                            false
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>