<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$name = (SITE_ID == 's1' ? $arResult['NAME'] : $arResult['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
$directionName = (SITE_ID == 's1' ? $arResult['DIRECTION']['NAME'] : $arResult['DIRECTION']['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
$prevText = (SITE_ID == 's1' ? $arResult['PREVIEW_TEXT'] : $arResult['PROPERTIES']['PREVIEW_TEXT']['~VALUE']['TEXT']);

$contentProperty = (SITE_ID == 's1' ? 'CONTENT' : 'CONTENT_' . strtoupper(SITE_ID));

$newsTags = (SITE_ID == 's1' ? $arResult['PROPERTIES']['SOURCES'] : $arResult['PROPERTIES']['SOURCES_' . strtoupper(SITE_ID)]);

//ShowViewContent
$this->SetViewTarget('VIDEO_BG'); ?>
<? if ($arResult['PROPERTIES']['VIDEO_BG']['VALUE']['path']): ?>
    <div class="bg" data-type="video">
        <div class="bg__inner">
            <div class="bg__inner-wrap">
                <video class="slides__video" autoplay="true" loop="true" muted="true" pip="false"
                       poster="<?= CFile::GetPath($arResult['PROPERTIES']['PHOTO_BG']['VALUE']) ?>" playsinline>
                    <source src="<?= $arResult['PROPERTIES']['VIDEO_BG']['VALUE']['path'] ?>"
                            type="video/mp4"
                            loop="true" muted="true">
                </video>
            </div>
        </div>
    </div>
<? elseif ($arResult['PROPERTIES']['PHOTO_BG']['VALUE']): ?>
    <div class="bg" data-type="img">
        <div class="bg__inner">
            <div class="bg__inner-wrap">
                <div class="img"
                     style="background-image: url(<?= CFile::GetPath($arResult['PROPERTIES']['PHOTO_BG']['VALUE']) ?>);"></div>
            </div>
        </div>
    </div>
<? endif; ?>
<? $this->EndViewTarget();
$this->SetViewTarget('LONG_NAME'); ?>
<div class="slides__label"><?= $directionName ?></div>
<div class="slides__heading heading heading_level-1"><?= $name ?></div>
<? $this->EndViewTarget();
// EndViewContent?>

<div class="news__column news__column_content">
    <div class="news__info">
        <div class="news__tag">
            <? if ($arResult['PROPERTIES']['DIRECTION']['VALUE']): ?>
                <a class="tag tag_light tag_xs"
                   href="<?= $arResult['LIST_PAGE_URL'] ?>?PROPERTY_DIRECTION=<?= $arResult['PROPERTIES']['DIRECTION']['VALUE'] ?>">
                    <svg class="tag__icon" width="40" height="40">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#tag"></use>
                    </svg>
                    <div class="tag__text"><?= $directionName ?></div>
                </a>
            <? endif; ?>
        </div>
        <? if ($arResult['DISPLAY_ACTIVE_FROM']): ?>
            <div class="news__info-item">
                <div class="news__info-inner">
                    <div class="news__info-icon">
                        <svg class="icon" width="21" height="23">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#calendar"></use>
                        </svg>
                    </div>
                    <div class="news__info-text">
                        <div class="news__info-label"><?= GetMessage('PUBLICATION_DATE') ?>:</div>
                        <div class="news__info-date"><?= $arResult['DISPLAY_ACTIVE_FROM'] ?></div>
                    </div>
                </div>
            </div>
        <? endif; ?>
        <? if ($arResult['PROPERTIES']['READ_TIME']['VALUE']): ?>
            <div class="news__info-item">
                <div class="news__info-inner">
                    <div class="news__info-icon">
                        <svg class="icon" width="23" height="23">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#time"></use>
                        </svg>
                    </div>
                    <div class="news__info-text">
                        <div class="news__info-label"><?= GetMessage('READ_TIME') ?>:</div>
                        <div class="news__info-date"><?= $arResult['PROPERTIES']['READ_TIME']['VALUE'] . ' '
                            . \IL\Utilities::getWord($arResult['PROPERTIES']['READ_TIME']['VALUE'], [
                                GetMessage('MINUTE_ONE'),
                                GetMessage('MINUTE_TWO'),
                                GetMessage('MINUTES_FIVE'),
                            ]) ?></div>
                    </div>
                </div>
            </div>
        <? endif; ?>
    </div>
    <? $APPLICATION->IncludeComponent(
        "sprint.editor:blocks",
        'press_center_detail',
        [
            "ELEMENT_ID" => $arResult["ID"],
            "IBLOCK_ID" => $arResult["IBLOCK_ID"],
            "PROPERTY_CODE" => $contentProperty,
        ],
        $component,
        [
            "HIDE_ICONS" => "Y",
        ]
    ); ?>
    <? if ($newsTags['VALUE']): ?>
        <h5><?= GetMessage('SOURCES') ?>:</h5>
        <p class="news__tags">
            <? foreach ($newsTags['VALUE'] as $key => $tag): ?>
                <? if ($newsTags['DESCRIPTION'][$key]): ?>
                    <a href="<?= $newsTags['DESCRIPTION'][$key] ?>" target="_blank"><?= $tag ?></a>
                <? else: ?>
                    <span><?= $tag ?></span>
                <? endif; ?>
                <?= ($tag == end($newsTags['VALUE']) ? false : ' / ') ?>
            <? endforeach; ?>
        </p>
    <? endif; ?>
    <? $APPLICATION->IncludeComponent(
        "bitrix:main.share",
        "share",
        [
            "HANDLERS" => [0 => "vk", 1 => "facebook", 2 => "ok"],
            "HIDE" => "N",
            "PAGE_TITLE" => "",
            "PAGE_URL" => "",
            "SHORTEN_URL_KEY" => "",
            "SHORTEN_URL_LOGIN" => "",
            "PAGE_URL" => $APPLICATION->GetCurPage(),
            "PAGE_TITLE" => $arResult['NAME'],
        ]
    ); ?>
</div>
