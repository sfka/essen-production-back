<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if ($arResult['PROPERTIES']['DIRECTION']['VALUE']) {
    $arSelect = ["ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_*"];
    $arFilter = ["IBLOCK_ID" => $arResult['PROPERTIES']['DIRECTION']['LINK_IBLOCK_ID'], 'ID' => $arResult['PROPERTIES']['DIRECTION']['VALUE'], 'ACTIVE' => 'Y'];
    $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arResult['DIRECTION'] = $arFields;
        $arProps = $ob->GetProperties();
        $arResult['DIRECTION']['PROPERTIES'] = $arProps;
    }
}