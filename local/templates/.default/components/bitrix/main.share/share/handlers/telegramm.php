<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

__IncludeLang(dirname(__FILE__)."/lang/".LANGUAGE_ID."/telegramm.php");
$name = "telegramm";
$title = GetMessage("BOOKMARK_HANDLER_TELEGRAMM");
$icon_url_template = '<a target="_blank" class="social__item" href="https://telegram.me/share/url?url=#PAGE_URL#&text=#PAGE_TITLE_UTF_ENCODED#" title="' . $title . '">
    <svg class="customClass" width="18px" height="15px">
        <use xlink:href="#telegram"></use>
    </svg>
</a>';
?>