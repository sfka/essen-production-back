<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="megamenu__list loop-list swiper-container">
    <div class="megamenu__list-inner loop-list__inner swiper-wrapper">

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</div>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
        <div class="megamenu__list-item loop-list__item swiper-slide">
            <a href="<?=$arItem["LINK"]?>" class="megamenu__list-link loop-list__link <?= ($arItem["SELECTED"]) ? 'selected' : false?>" data-hover="<?=$arItem["TEXT"]?>"><?=$arItem["TEXT"]?></a>
            <ul class="megamenu__list-dropdown">
		<?else:?>
                <li class="megamenu__list-dropdown-item">
                    <a class="megamenu__list-dropdown-link <?= ($arItem["SELECTED"] ? 'item-selected' : false) ?>" href="<?=$arItem["LINK"]?>" data-hover="<?=$arItem["TEXT"]?>"><?=$arItem["TEXT"]?></a>
                </li>
		<?endif?>
	<?else:?>
		<?if ($arItem["PERMISSION"] > "D"):?>
			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
                <div class="megamenu__list-item loop-list__item swiper-slide">
                    <a href="<?=$arItem["LINK"]?>" class="megamenu__list-link loop-list__link <?= ($arItem["SELECTED"]) ? 'selected' : false?>" data-hover="<?=$arItem["TEXT"]?>"><?=$arItem["TEXT"]?></a>
                </div>
			<?else:?>
                <li class="megamenu__list-dropdown-item">
                    <a class="megamenu__list-dropdown-link <?= ($arItem["SELECTED"] ? 'item-selected' : false) ?>" href="<?=$arItem["LINK"]?>" data-hover="<?=$arItem["TEXT"]?>"><?=$arItem["TEXT"]?></a>
                </li>
			<?endif?>
		<?endif?>
	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul>", ($previousLevel-1) );?>
<?endif?>
    </div>
</div>
<?endif?>