<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="navigation">
        <? foreach ($arResult as $arItem): ?>
            <a class="navigation__item" href="<?= $arItem['LINK'] ?>">
                <div class="navigation__wrap">
                    <div class="inner"><span class="title"><?= $arItem['TEXT'] ?></span>
                        <svg class="icon" width="24" height="16">
                            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#navigation"></use>
                        </svg>
                    </div>
                </div>
            </a>
        <? endforeach ?>
    </div>
<? endif ?>