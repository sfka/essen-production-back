<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="header__main-nav">
    <nav class="nav-menu">
<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</div>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
        <div class="nav-menu__item">
            <a href="<?=$arItem["LINK"]?>" class="nav-menu__link <?= ($arItem["SELECTED"]) ? 'selected' : false?>" data-hover="<?=$arItem["TEXT"]?>"><?=$arItem["TEXT"]?></a>
            <ul class="nav-menu__dropdown">
		<?else:?>
                <li class="nav-menu__dropdown-item">
                    <a class="nav-menu__dropdown-link <?= ($arItem["SELECTED"] ? 'item-selected' : false) ?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                </li>
		<?endif?>
	<?else:?>
		<?if ($arItem["PERMISSION"] > "D"):?>
			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
                <div class="nav-menu__item">
                    <a href="<?=$arItem["LINK"]?>" class="nav-menu__link <?= ($arItem["SELECTED"]) ? 'selected' : false?>" data-hover="<?=$arItem["TEXT"]?>"><?=$arItem["TEXT"]?></a>
                </div>
			<?else:?>
                <li class="nav-menu__dropdown-item">
                    <a class="nav-menu__dropdown-link <?= ($arItem["SELECTED"] ? 'item-selected' : false) ?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                </li>
			<?endif?>
		<?endif?>
	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul>", ($previousLevel-1) );?>
<?endif?>
    </nav>
</div>
<?endif?>