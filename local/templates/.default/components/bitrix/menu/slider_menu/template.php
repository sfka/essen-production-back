<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div class="page__list scrollable-list swiper-container" data-view="wide" data-filter="not">
        <div class="swiper-wrapper">
            <? foreach ($arResult as $arItem): ?>
                <? $name = (SITE_ID == 's1' ? $arItem['DESC']['NAME'] : $arItem['DESC']['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
                $longName = (SITE_ID == 's1' ? $arItem['DESC']['PROPERTIES']['LONG_NAME']['VALUE'] : $arItem['DESC']['PROPERTIES']['LONG_NAME_' . strtoupper(SITE_ID)]['VALUE']);
                ?>
                <div class="swiper-slide">
                    <a class="news-card card" href="<?= $arItem['LINK'] ?>" data-type="company">
                        <div class="news-card__img-wrapper">
                            <div class="news-card__mask"></div>
                            <div class="news-card__img"
                                 style="background-image: url(<?= CFile::GetPath($arItem['DESC']['PROPERTIES']['COMPANY_PHOTO']['VALUE']) ?>)"></div>
                        </div>
                        <div class="news-card__inner card__inner">
                            <div class="news-card__text">
                                <div class="news-card__title"><?= $name ?></div>
                                <div class="news-card__desc"><?= $longName ?></div>
                            </div>
                            <div class="news-card__btn card__btn">
                                <svg class="card__btn-icon" width="36" height="24">
                                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                                </svg>
                            </div>
                        </div>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? endif ?>