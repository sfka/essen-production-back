<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (CModule::IncludeModule("iblock")) {
    foreach ($arResult as $key => $arItem) {
        if (isset($arItem['PARAMS']['IBLOCK_ID'])) {
            $arSelect = ["ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_*"];
            $arFilter = ["IBLOCK_ID" => $arItem['PARAMS']['IBLOCK_ID'], 'ACTIVE' => 'Y'];
            $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $arResult[$key]['DESC'] = $arFields;
                $arProps = $ob->GetProperties();
                $arResult[$key]['DESC']['PROPERTIES'] = $arProps;
            }
        }
    }
}