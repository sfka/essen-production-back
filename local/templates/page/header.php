<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__); ?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>" data-theme="light">

<head>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/system/head.php', [], ['SHOW_BORDER' => false]) ?>
    <? $APPLICATION->ShowHead(); ?>
</head>

<body class="loading" data-barba="wrapper" data-temp="page">
<? if ($_REQUEST['SHOW_ADMIN']): ?>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<? endif; ?>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/header/layer.php', [], ['SHOW_BORDER' => false]) ?>
<div class="main-wrapper" data-barba="container" data-barba-namespace="page">
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/system/header.php', ['HEADER_CLASS' => 'header_inner-active header_page'], ['SHOW_BORDER' => false]) ?>
    <div class="container">
        <div class="page">
            <div class="page__heading"><?= $APPLICATION->ShowTitle() ?></div>