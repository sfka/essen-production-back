<div class="page__bottom-row page__bottom-row_hide">
    <div class="page__controls">
        <div class="slider-controls">
            <div class="scrollable-list__btn scrollable-list__btn-prev slider-btn">
                <svg class="slider-btn__icon" width="18" height="12">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-left"></use>
                </svg>
            </div>
            <div class="scrollable-list__btn scrollable-list__btn-next slider-btn">
                <svg class="slider-btn__icon" width="18" height="12">
                    <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                </svg>
            </div>
        </div>
    </div>
    <? $APPLICATION->ShowViewContent('footer_add_blocks'); ?>
    <div class="page__scroller">
        <div class="scroller">
            <div class="scroller__text"><?= GetMessage('SCROLL') ?></div>
            <div class="scroller__line">
                <div class="scroller__circle"></div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/popup.php', [], ['SHOW_BORDER' => false]) ?>
</div>
</body>
</html>