<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__); ?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>" data-theme="light">

<head>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/system/head.php', [], ['SHOW_BORDER' => false]) ?>
    <? $APPLICATION->ShowHead(); ?>
</head>

<body class="loading" data-barba="wrapper" data-temp="content">
<? if ($_REQUEST['SHOW_ADMIN']): ?>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<? endif; ?>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/header/layer.php', [], ['SHOW_BORDER' => false]) ?>
<div class="main-wrapper" data-barba="container" data-barba-namespace="content">
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/system/header.php', ['HEADER_CLASS' => 'header_inner-active header_detail-active'], ['SHOW_BORDER' => false]) ?>
    <div class="container">
        <div class="content">
            <div class="content__bg">
                <div class="bg" data-type="letters">
                    <div class="bg__inner">
                        <div class="bg__inner-wrap">
                            <span class="letter">E</span>
                            <span class="letter">S</span>
                            <span class="letter">S</span>
                            <span class="letter">E</span>
                            <span class="letter">N</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content__wrapper">
                <div class="content__inner scroll-content">
                    <div class="content-inner">