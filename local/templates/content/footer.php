<? $APPLICATION->IncludeComponent(
    "bitrix:menu",
    "bottom_menu",
    [
        "ALLOW_MULTI_SELECT" => "N",
        "CHILD_MENU_TYPE" => "left",
        "DELAY" => "N",
        "MAX_LEVEL" => "1",
        "MENU_CACHE_GET_VARS" => [
        ],
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "ROOT_MENU_TYPE" => "bottom",
        "USE_EXT" => "N",
        "COMPONENT_TEMPLATE" => "bottom_menu",
    ],
    false
); ?>
</div> <? // content-inner ?>
</div> <? // content__inner scroll-content ?>
</div> <? // content__wrapper ?>
</div> <? // content ?>
</div> <? // container ?>
<div class="scroll-up">
    <svg class="complete" width="64" height="64" viewport="0 0 32 32">
        <defs>
            <linearGradient id="grad">
                <stop offset="0%" stop-color="#0a1f8f"></stop>
                <stop offset="100%" stop-color="#0a1f8f"></stop>
            </linearGradient>
        </defs>
        <circle cx="32" cy="32" r="28"></circle>
        <circle class="progress-bar" cx="32" cy="32" r="28"></circle>
    </svg>
    <svg class="scroll-up__icon" width="32" height="32">
        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#scroll-up"></use>
    </svg>
</div>
</div> <? // main-wrapper ?>
</body>

</html>