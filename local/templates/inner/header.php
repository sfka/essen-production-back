<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__); ?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>" data-theme="light">

<head>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/system/head.php', [], ['SHOW_BORDER' => false]) ?>
    <? $APPLICATION->ShowHead(); ?>
</head>

<body class="loading" data-barba="wrapper" data-temp="inner">
<? if ($_REQUEST['SHOW_ADMIN']): ?>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<? endif; ?>
<? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/header/layer.php', [], ['SHOW_BORDER' => false]) ?>
<div class="main-wrapper" data-barba="container" data-barba-namespace="slider-inner">
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/system/header.php', ['TITLE_IN_HEADER' => 'Y', 'HEADER_CLASS' => 'header_inner-active'], ['SHOW_BORDER' => false]) ?>
    <div class="container">
        <div class="slides slides_inner-active swiper-container">
            <div class="slides__inner swiper-wrapper">
                <div class="slides__item swiper-slide">
                    <div class="slides__bg slides__bg_active">
                        <? $APPLICATION->ShowViewContent('VIDEO_BG'); ?>
                    </div>
                    <div class="slides__content">
                        <div class="slides__line">
                            <? $APPLICATION->ShowViewContent('LONG_NAME'); ?>
                        </div>
                        <div class="slides__line">
                            <div class="slides__row">
                                <? $APPLICATION->ShowViewContent('NUMBERS'); ?>
                                <a class="main-button main-button_active magnetic-wrap"
                                   href="javascript:void(0)">
                                    <span class="main-button__text main-button__default-text"><?= $APPLICATION->ShowTitle() ?></span>
                                    <span class="main-button__text main-button__hidden-text"><?= GetMessage('MORE') ?></span>
                                </a>
                            </div>
                        </div>
                        <div class="slides__inner-content scroll-content">
                            <div class="content-inner">
                                <div class="<?= ($_SERVER['REAL_FILE_PATH'] == '/press-center/index.php' ? 'offset-left' : 'offset-center') ?>">
                                    <div class="content-title">
                                        <h3><?= $APPLICATION->ShowTitle() ?></h3>
                                    </div>
