<? use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest(); ?>
<? $APPLICATION->IncludeComponent(
    "bitrix:menu",
    "bottom_menu",
    [
        "ALLOW_MULTI_SELECT" => "N",
        "CHILD_MENU_TYPE" => "left",
        "DELAY" => "N",
        "MAX_LEVEL" => "1",
        "MENU_CACHE_GET_VARS" => [
        ],
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "ROOT_MENU_TYPE" => "bottom",
        "USE_EXT" => "N",
        "COMPONENT_TEMPLATE" => "bottom_menu",
    ],
    false
); ?>
</div> <? //offset-center ?>
</div> <? //content-inner ?>
</div> <? //slides__inner-content scroll-content ?>
</div> <? //slides__content ?>
</div> <? //slides__item swiper-slide ?>
</div> <? //slides__inner swiper-wrapper ?>
<div class="slides__controls slider-controls">
    <div class="slides__prev-btn slider-btn">
        <svg class="slider-btn__icon" width="18" height="12">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-left"></use>
        </svg>
    </div>
    <div class="slides__next-btn slider-btn">
        <svg class="slider-btn__icon" width="18" height="12">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
        </svg>
    </div>
</div>
</div> <? // slides slides_inner-active swiper-container?>


<?
if ($ajax && $request->get('VACANCY_ID')) {
    $APPLICATION->RestartBuffer();
    if ($request->get('AJAX_MODAL')) {
        $ajaxModal = 'Y';
    }
    if ($request->get('AJAX_MODAL') == 'Y' && $request->get('VACANCY_ID')) {
        $arSelect = ["ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_*"];
        $arFilter = ["IBLOCK_ID" => IBLOCK_LEADERSHIP, 'ID' => $request->get('VACANCY_ID'), 'ACTIVE' => 'Y'];
        $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $arFields['PROPERTIES'] = $arProps;
        }
        $arVacancyDetail = $arFields;
    }
}
?>
<? // модалка конкретного лидера ?>
<div class="popup-wrapper" id="popupTeam">
    <div class="popup-wrapper__close popup-close magnetic-wrap">
        <svg class="popup-wrapper__close-icon" width="22" height="22">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </div>
    <div class="popup-wrapper__bg">
        <div class="popup-wrapper__bg-left"></div>
        <div class="popup-wrapper__bg-right"></div>
    </div>
    <div class="popup-wrapper__inner ajax__update_content">
        <? if ($ajaxModal == 'Y' && $arVacancyDetail): ?>
            <? $name = (SITE_ID == 's1' ? $arVacancyDetail['NAME'] : $arVacancyDetail['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
            $position = (SITE_ID == 's1' ? $arVacancyDetail['PROPERTIES']['POSITION']['VALUE'] : $arVacancyDetail['PROPERTIES']['POSITION_' . strtoupper(SITE_ID)]['VALUE']);
            $biography = (SITE_ID == 's1' ? $arVacancyDetail['PROPERTIES']['BIOGRAPHY']['~VALUE']['TEXT'] : $arVacancyDetail['PROPERTIES']['BIOGRAPHY_' . strtoupper(SITE_ID)]['VALUE']);
            ?>
            <div class="popup-wrapper__left-col">
                <div class="content-container">
                    <div class="popup-team">
                        <div class="popup-team__img">
                            <img src="<?= CFile::GetPath($arVacancyDetail['PREVIEW_PICTURE']) ?>"
                                 alt="">
                        </div>
                        <div class="popup-team__content">
                            <h2><?= $name ?></h2><span><?= $position ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="popup-wrapper__right-col">
                <div class="content-container">
                    <h2><?= GetMessage('BIOGRAPHY') ?></h2>
                    <?= $biography ?>
                </div>
            </div>
        <? endif; ?>
    </div>
</div>
<? if ($ajax) die(); ?>
</div> <? // <div class="container"> ?>
<div class="scroll-up">
    <svg class="complete" width="64" height="64" viewport="0 0 32 32">
        <defs>
            <linearGradient id="grad">
                <stop offset="0%" stop-color="#0a1f8f"></stop>
                <stop offset="100%" stop-color="#0a1f8f"></stop>
            </linearGradient>
        </defs>
        <circle cx="32" cy="32" r="28"></circle>
        <circle class="progress-bar" cx="32" cy="32" r="28"></circle>
    </svg>
    <svg class="scroll-up__icon" width="32" height="32">
        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#scroll-up"></use>
    </svg>
</div>
</div> <? // main-wrapper ?>
</body>

</html>