<?
if ($_POST && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
    $obForm = new \IL\FormContacts(IBLOCK_FORM_CONTACTS);
    $arFields = $_POST;
    $arResult = $obForm->add($arFields);
    if ($arResult === true) {
        echo json_encode([
            'status' => 'ok',
            'title' => 'Спасибо, вы успешно записались!',
            'message' => 'Администратор салона «Качество жизни» свяжется с вами в ближайшее время.',
        ]);
    } else {
        echo json_encode([
            'status' => 'error',
            'message' => $arResult
        ]);
    }
}