<?

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\HttpApplication,
    Bitrix\Main\Loader,
    Bitrix\Main\IO,
    Bitrix\Main\Application,
    Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);
CJSCore::Init(['jquery']);
if (!CModule::IncludeModule("iblock"))
    return;

$request = HttpApplication::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request['mid']) != '' ? $request['mid'] : $request['id'];
Loader::includeModule($module_id);

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = [];
$rsIBlock = CIBlock::GetList(["SORT" => "ASC"], ["ACTIVE" => "Y"]);
while ($arr = $rsIBlock->Fetch()) {
    $arIBlock[$arr["ID"]] = $arr["NAME"] . "[" . $arr['ID'] . "] - " . $arr['IBLOCK_TYPE_ID'];
}

$rsSites = CSite::GetList($by = "sort", $order = "asc");
while ($arSite = $rsSites->Fetch()) {
    $siteList[] = $arSite;
}

$aTabs = [
    [
        "DIV" => "edit",
        "TAB" => 'Выгрузка форм в csv',
        "TITLE" => 'Выгрузка форм в csv',
        "OPTIONS" => [
            Loc::getMessage("IZI_EXPORT_OPTIONS_IBLOCKS"),
            [
                "iblocks",
                Loc::getMessage("IZI_EXPORT_OPTIONS_IBLOCKS_TITLE"),
                '',
                ["multiselectbox", $arIBlock],
            ],
        ],
    ],
];
if ($request->isPost() && check_bitrix_sessid()) {
    $postList = [];
    foreach ($request->getPostList() as $key => $item) {
        $postList[$key] = $item;
    }

    foreach ($aTabs as $aTab) {
        foreach ($aTab["OPTIONS"] as $arOption) {
            if (!is_array($arOption) || $arOption["note"]) continue;
            if ($request["apply"]) {
                $optionValue = $request->getPost($arOption[0]);
                Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
            } elseif ($request["export"]) {
                $optionValue = $request->getPost($arOption[0]);
                Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
                $arFormsId = explode(',', Option::get($module_id, 'iblocks'));
                $arResult = createArray($arFormsId);
                $fileName = $_SERVER['DOCUMENT_ROOT'] . '/upload/forms_' . date('d.m.Y') . '.csv';
                create_csv_file($arResult, $fileName);


                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private", false); // нужен для Explorer
                header("Content-Type: csv");
                header("Content-Disposition: attachment; filename=\"" . basename($fileName) . "\";");
                header("Content-Transfer-Encoding: binary");
// нужно сделать подсчет размера файла его пути
                header("Content-Length: " . filesize($fileName));
                readfile($fileName);
                exit();
            }
        }
    }
    LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . $module_id . "&lang=" . LANG);
}

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin(); ?>

<form action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= $module_id ?>&lang=<?= LANG ?>" method="POST">
    <?= bitrix_sessid_post() ?>
    <? foreach ($aTabs as $aTab) {

        if ($aTab["OPTIONS"]) {

            $tabControl->BeginNextTab();

            __AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
        }
    } ?>
    <? //ShowParamsHTMLByArray($arAllOptions["main"]); ?>
    <? $tabControl->Buttons(); ?>
    <input type="submit" name="apply" value="<? echo(Loc::GetMessage("IZI_EXPORT_OPTIONS_INPUT_APPLY")); ?>"
           class="adm-btn-save"/>
    <input type="submit" name="export" value="<? echo(Loc::GetMessage("IZI_EXPORT_OPTIONS_INPUT_EXPORT")); ?>"/>
</form>
<? $tabControl->End(); ?>

<?
function createArray($arFormsId)
{
    /*$arPropCodes = [
        'PROPERTY_SURNAME', 'PROPERTY_NAME', 'PROPERTY_PATRONYMIC', 'PROPERTY_PHONE', 'PROPERTY_EMAIL',
    ];*/
    foreach ($arFormsId as $formId) {
        $properties = CIBlockProperty::GetList(["sort" => "asc", "name" => "asc"], ["ACTIVE" => "Y", "IBLOCK_ID" => $formId]);
        while ($prop_fields = $properties->GetNext()) {
            $arPropCodes[] = 'PROPERTY_' . $prop_fields['CODE'];
            $arPropCodesShowed[$prop_fields['CODE']] = $prop_fields['NAME'];
        }
    }
    $arPropCodes = array_unique($arPropCodes);
    $arPropCodesShowed = array_unique($arPropCodesShowed);
    $arResult = [];
    $arSelect = array_merge(["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM"], $arPropCodes);
    $arPropCodesShowed = array_merge(["DATE_ACTIVE_FROM"], $arPropCodesShowed);
    $arResult[] = $arPropCodesShowed;
    foreach ($arFormsId as $formId) {
        $arFilter = ["IBLOCK_ID" => IntVal($formId), '!PROPERTY_UPLOADED' => 'Y'];
        $res = CIBlockElement::GetList(['DATE_ACTIVE_FROM' => 'DESC'], $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arResultFields = [];
            $arFields = $ob->GetFields();
            foreach ($arSelect as $field) {
                if (in_array($field, ["ID", "IBLOCK_ID", "NAME",]))
                    continue;
                if (strpos($field, 'PROPERTY') !== false) {
                    $field = $field . '_VALUE';
                }
                if (is_array($arFields[$field]) && $arFields[$field]['TEXT']) {
                    $arResultFields[] = $arFields[$field]['TEXT'];
                } else {
                    $arResultFields[] = $arFields[$field];
                }
            }
            array_push($arResult, $arResultFields);
            CIBlockElement::SetPropertyValues($arFields['ID'], $arFields['IBLOCK_ID'], 'Y', 'UPLOADED');
        }
    }
    return $arResult;
}

function create_csv_file($create_data, $file = null, $col_delimiter = ';', $row_delimiter = "\r\n")
{

    if (!is_array($create_data))
        return false;

    if ($file && !is_dir(dirname($file)))
        return false;

    // строка, которая будет записана в csv файл
    $CSV_str = '';

    // перебираем все данные
    foreach ($create_data as $row) {
        $cols = [];

        foreach ($row as $col_val) {
            // строки должны быть в кавычках ""
            // кавычки " внутри строк нужно предварить такой же кавычкой "
            if ($col_val && preg_match('/[",;\r\n]/', $col_val)) {
                // поправим перенос строки
                if ($row_delimiter === "\r\n") {
                    $col_val = str_replace("\r\n", '\n', $col_val);
                    $col_val = str_replace("\r", '', $col_val);
                } elseif ($row_delimiter === "\n") {
                    $col_val = str_replace("\n", '\r', $col_val);
                    $col_val = str_replace("\r\r", '\r', $col_val);
                }

                $col_val = str_replace('"', '""', $col_val); // предваряем "
                $col_val = '"' . $col_val . '"'; // обрамляем в "
            }

            $cols[] = $col_val; // добавляем колонку в данные
        }

        $CSV_str .= implode($col_delimiter, $cols) . $row_delimiter; // добавляем строку в данные
    }

    $CSV_str = rtrim($CSV_str, $row_delimiter);

    // задаем кодировку
    if ($file) {
        $CSV_str = iconv("UTF-8", "UTF-8", $CSV_str);

        $done = file_put_contents($file, $CSV_str);

        return $done ? $CSV_str : false;
    }

    return $CSV_str;

}

?>
<script>
    $('select[name="iblocks[]"]').attr('size', '<?= count($arIBlock) ?>');
</script>