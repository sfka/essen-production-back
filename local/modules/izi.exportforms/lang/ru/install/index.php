<?
$MESS["IZI_EXPORT_NAME"] = "Выгрузка всех форм";
$MESS["IZI_EXPORT_DESCRIPTION"] = "Выгрузка всех форм";
$MESS["IZI_EXPORT_PARTNER_NAME"] = "izi";
$MESS["IZI_EXPORT_PARTNER_URI"] = "http://ilartech.com/";
$MESS["IZI_EXPORT_ISNTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["IZI_EXPORT_INSTALL_TITLE"] = "Установка модуля";
$MESS["IZI_EXPORT_UNINSTALL_TITLE"] = "Деинсталляция модуля";