<?

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Config\Option,
    Bitrix\Main\EventManager,
    Bitrix\Main\Application,
    Bitrix\Main\IO\Directory;

Loc::loadMessages(__FILE__);

class izi_exportForms extends CModule
{

    public function __construct()
    {

        if (file_exists(__DIR__ . "/version.php")) {

            $arModuleVersion = [];

            include_once(__DIR__ . "/version.php");

            $this->MODULE_ID = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME = Loc::getMessage("IZI_EXPORT_NAME");
            $this->MODULE_DESCRIPTION = Loc::getMessage("IZI_EXPORT_DESCRIPTION");
            $this->PARTNER_NAME = Loc::getMessage("IZI_EXPORT_PARTNER_NAME");
            $this->PARTNER_URI = Loc::getMessage("IZI_EXPORT_PARTNER_URI");
        }

        return false;
    }

    public function DoInstall()
    {
        global $APPLICATION;

        if (CheckVersion(ModuleManager::getVersion("main"), "14.0.0")) {
            $this->InstallFiles();
            $this->InstallDB();

            ModuleManager::registerModule($this->MODULE_ID);
            $this->InstallEvents();
        } else {
            $APPLICATION->ThrowException(
                Loc::getMessage('IZI_EXPORT_ISNTALL_ERROR_VERSION')
            );
        }

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("IZI_EXPORT_INSTALL_TITLE") . " \"" . Loc::getMessage("IZI_EXPORT_NAME") . "\"", __DIR__ . "/step.php"
        );

        return false;
    }

    public function InstallFiles()
    {

        return false;
    }

    public function InstallDB()
    {

        return false;
    }

    public function InstallEvents()
    {

        EventManager::getInstance()->registerEventHandler(
            "main",
            "OnBeforeEndBufferContent",
            $this->MODULE_ID,
            "Izi\ExportForm\Main",
            "appendScriptsToPage"
        );

        return false;
    }

    public function DoUninstall()
    {

        global $APPLICATION;

        $this->UnInstallFiles();
        $this->UnInstallDB();
        $this->UnInstallEvents();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("IZI_EXPORT_UNINSTALL_TITLE") . " \"" . Loc::getMessage("FALBAR_TOTOP_NAME") . "\"",
            __DIR__ . "/unstep.php"
        );

        return false;
    }

    public function UnInstallDB()
    {

        Option::delete($this->MODULE_ID);

        return false;
    }

    public function UnInstallEvents()
    {

        EventManager::getInstance()->unRegisterEventHandler(
            "main",
            "OnBeforeEndBufferContent",
            $this->MODULE_ID,
            "Izi\ExportForm\Main",
            "appendScriptsToPage"
        );

        return false;
    }
}