<?

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\HttpApplication,
    Bitrix\Main\Loader,
    Bitrix\Main\IO,
    Bitrix\Main\Application,
    Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);
CJSCore::Init(['jquery']);

$request = HttpApplication::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request['mid']) != '' ? $request['mid'] : $request['id'];
Loader::includeModule($module_id);

$rsSites = CSite::GetList($by = "sort", $order = "asc");
while ($arSite = $rsSites->Fetch()) {
    $siteList[] = $arSite;
}

$aTabs = [
    [
        'DIV' => 'edit_s1',
        'SITE' => 's1',
        'TAB' => 'Kazan Expo',
        'TITLE' => 'Файл перевода на Kazan Expo',
        'OPTIONS' => [
            'TITLE' => Loc::getMessage("IZI_LANG_OPTIONS_FILE"),
            'OPTIONS' => [
                "file_s1",
                Loc::getMessage("IZI_LANG_OPTIONS_FILE_LOAD"),
                "/local/include/lang/",
                [
                    "text",
                    "80",
                ],
            ],
        ],
    ],
];
if ($request->isPost() && check_bitrix_sessid()) {
    $postList = [];
    foreach ($request->getPostList() as $key => $item) {
        $postList[$key] = $item;
    }

    foreach ($aTabs as $aTab) {
        foreach ($aTab["OPTIONS"] as $arOption) {
            if (!is_array($arOption) || $arOption["note"]) continue;

            if ($request["apply"]) {

                $optionValue = $request->getPost($arOption[0]);
                if (!empty($optionValue)) {

                    $dir = new IO\Directory(Application::getDocumentRoot() . $optionValue);
                    if (!$dir->isExists()) {
                        Option::set($module_id, $arOption[0], '');
                    } else {
                        Option::set($module_id, $arOption[0], $optionValue);

                        //Проверим языковые файлы по списку сайтов
                        foreach ($siteList as $keySite => $arSite) {
                            $file = new IO\File(Application::getDocumentRoot() . $optionValue . $arSite['LANGUAGE_ID'] . '.php');
                            //Если файла нет, то создадим с ключами из первого найденного языкового файла и пустыми значениями
                            if (!$file->isExists()) {
                                $MESS = [];
                                $fileFirst = new IO\File(Application::getDocumentRoot() . $optionValue . $siteList[0]['LANGUAGE_ID'] . '.php');
                                include($fileFirst->getPath());
                                foreach ($MESS as $key => $val) {
                                    $str .= "\n" . '$MESS["' . $key . '"]' . ' = ' . '""' . ";";
                                }
                                $file = IO\File::putFileContents(Application::getDocumentRoot() . $optionValue . $arSite['LANGUAGE_ID'] . '.php', "<?" . $str);
                                unset($MESS);
                            }
                        }

                        //Получим новые поля и удалим из POST-запроса
                        foreach ($postList['trans_key'] as $keyPost => $post) {
                            $postList['trans_' . mb_strtoupper($post)] = $postList['trans_val'][$keyPost];
                        }
                        unset($postList['trans_key'], $postList['trans_val']);

                        //Пересоберем массив по языкам
                        $arPost = [];
                        foreach ($postList as $keyPost => $post) {
                            if (is_array($post)) {
                                foreach ($post as $lang => $item) {
                                    $arPost[$lang][str_replace('trans_', '', $keyPost)] = $item;
                                }
                            }
                        }

                        //Запись в файл
                        foreach ($arPost as $keyPost => $valPost) {
                            $str = '';
                            foreach ($valPost as $key => $val) {
                                $str .= "\n" . '$MESS["' . $key . '"]' . ' = ' . '"' . str_replace("\"", '\'', $val) . '"' . ";";
                            }
                            IO\File::putFileContents(Application::getDocumentRoot() . $optionValue . $keyPost . '.php', "<?" . $str);
                        }
                    }
                }
            } elseif ($request["default"]) {
                Option::set($module_id, $arOption[0], $arOption[2]);
            }
        }
    }
    LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . $module_id . "&lang=" . LANG);
}

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin(); ?>

<form action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= $module_id ?>&lang=<?= LANG ?>" method="POST">
    <input type="hidden" id="tabControl_active_tab" name="tabControl_active_tab" value="edit_s1">
    <?
    foreach ($aTabs as $aTab) {
        if ($aTab["OPTIONS"]) {
            $tabControl->BeginNextTab();
            __AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
        }

        //Получим содержимое файлов и сгруппируем по ключу
        $optionValue = COption::GetOptionString($module_id, "file_" . $aTab['SITE']);
        $arMESS = [];
        foreach ($siteList as $keySite => $arSite) {
            $file = new IO\File(Application::getDocumentRoot() . $optionValue . $arSite['LANGUAGE_ID'] . '.php');
            if ($file->isExists()) {
                $MESS = [];
                include($file->getPath());
                foreach ($MESS as $key => $value) {
                    $arMESS[$key][str_replace('.php', '', $file->getName())] = $value;
                }
                unset($MESS);
            }
        }
        foreach ($arMESS as $key => $arValues):?>
            <? if (!empty($arValues)): ?>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td width="90%">
                                    <a href="javascript:void(0);" style="color: red; padding-right: 5px; text-decoration: none;" onclick="delInput(this)">X</a>
                                </td>
                                <td width="10%">
                                    <input type="text" disabled name="mess_<?= $key ?>" value="<?= $key ?>">
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <? foreach ($arValues as $keyValue => $value) : ?>
                                <tr>
                                    <td><?= $keyValue ?></td>
                                    <td>
                                        <input type="text" name="trans_<?= $key ?>[<?= $keyValue ?>]" value="<?= htmlspecialchars($value) ?>" size="80">
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        </table>
                    </td>
                </tr>
            <? endif; ?>
        <? endforeach; ?>
        <tr>
            <td>
                <input type="button" value="<?= Loc::GetMessage("IZI_LANG_OPTIONS_INPUT_ADD") ?>" onclick="addNewInputForMess(this)">
            </td>
        </tr>
    <? }
    $tabControl->Buttons();
    ?>
    <input type="submit" name="apply" value="<? echo(Loc::GetMessage("IZI_LANG_OPTIONS_INPUT_APPLY")); ?>" class="adm-btn-save"/>
    <input type="submit" name="default" value="<? echo(Loc::GetMessage("IZI_LANG_OPTIONS_INPUT_DEFAULT")); ?>"/>
    <?= bitrix_sessid_post() ?>
</form>
<? $tabControl->End(); ?>

<script>
    var currentCount = 0;

    function addNewInputForMess(a) {
        currentCount++;
        var row = jsUtils.FindParentObject(a, "tr");
        var tbl = row.parentNode;

        var tableRow = tbl.rows[row.rowIndex - 1].cloneNode(true);
        tbl.insertBefore(tableRow, row);

        var inputLeft = jsUtils.FindChildObject(tableRow.cells[0], "input", null, true);
        inputLeft.name = "trans_key[" + currentCount + ']';
        inputLeft.value = "";
        inputLeft.disabled = false;

        var inputs = $(tableRow.cells[1]).find('input');
        $.each(inputs, function () {
            var lang = $(this).closest('tr').find('td').html();
            $(this).attr('name', 'trans_val[' + currentCount + '][' + lang + ']').val('');
        });
    }

    function delInput(a) {
        var row = jsUtils.FindParentObject(a, "table");
        var tbl = row.parentNode.parentNode;
        tbl.remove();
    }

    if (!window.tabControl || !BX.is_subclass_of(window.tabControl, BX.adminTabControl))
        window.tabControl = new BX.adminTabControl("tabControl", "tabControl_" + BX.bitrix_sessid, [{'DIV': 'edit_s1'}, {'DIV': 'edit_en'}, {'DIV': 'edit_ch'}], []);
    else if (!!window.tabControl)
        window.tabControl.PreInit(true);

</script>