<?
$MESS["IZI_LANG_NAME"] = "Языковые файлы";
$MESS["IZI_LANG_DESCRIPTION"] = "Редактирование языковых файлов из админки";
$MESS["IZI_LANG_PARTNER_NAME"] = "izi";
$MESS["IZI_LANG_PARTNER_URI"] = "http://ilartech.com/";
$MESS["IZI_LANG_ISNTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["IZI_LANG_INSTALL_TITLE"] = "Установка модуля";
$MESS["IZI_LANG_UNINSTALL_TITLE"] = "Деинсталляция модуля";