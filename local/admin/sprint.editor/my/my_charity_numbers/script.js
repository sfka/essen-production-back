sprint_editor.registerBlock('my_charity_numbers', function ($, $el, data) {

    data = $.extend({
        valueCount: '',
        valueTitle: '',
        valueDesc: '',
        valueQuote: '',
    }, data);

    this.getData = function () {
        return data;
    };

    this.collectData = function () {
        data.valueCount = $el.find('.sp-izi-count').val();
        data.valueTitle = $el.find('.sp-izi-count-title').val();
        data.valueDesc = $el.find('.sp-izi-textarea').val();
        data.valueQuote = $el.find('.sp-izi-quote-textarea').val();
        return data;
    };

    this.afterRender = function () {
        var $input = data.value = $el.find('.sp-textarea');
    };
});
