sprint_editor.registerBlock('my_history_image_with_h', function ($, $el, data) {
    var areas = [
        {
            dataKey: 'image',
            blockName: 'image',
            container: '.sp-area1'
        },
        {
            dataKey: 'htag',
            blockName: 'htag',
            container: '.sp-x-box-block'
        }
    ];

    this.getData = function () {
        return data;
    };

    this.collectData = function () {
        return data;
    };

    this.getAreas = function () {
        return areas;
    };
});
