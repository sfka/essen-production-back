sprint_editor.registerBlock('my_history_contents', function ($, $el, data) {

    data = $.extend({
        selectors: [],
        elements: [] /* */
    }, data);

    var all = ['my_history_image_with_h'];

    var checked = [];
    $.each(all, function (index, val) {
        var selected = ($.inArray(val, data.selectors) >= 0);
        checked.push({
            id: val,
            title: val,
            selected: selected
        })

    });
    this.getData = function () {
        data['checked'] = checked;
        return data;
    };

    this.collectData = function () {
        data.selectors = [];

        $el.find('input[type=checkbox]').each(function () {
            var $obj = $(this);
            if ($obj.is(':checked')) {
                var val = $obj.val();
                if (jQuery.inArray(val, all) >= 0) {
                    data.selectors.push($obj.val());
                }
            }
        });
        data.elements = [];
        $('.sp-block-my_history_image_with_h').each(function () {
            var obj = {};
            obj['text'] = $(this).find('.sp-block-htag input').val();
            obj['long_text'] = $(this).find('input.sp-item-text').val();
            obj['anchor'] = $(this).find('.sp-block-htag .sp-anchor').text();
            data.elements.push(obj);
        });


        delete data['checked'];
        return data;
    };

    this.afterRender = function () {

    };

});
