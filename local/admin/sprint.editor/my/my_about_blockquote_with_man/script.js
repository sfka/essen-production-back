sprint_editor.registerBlock('my_about_blockquote_with_man', function ($, $el, data) {
    var areas = [
        {
            dataKey: 'image3',
            blockName: 'image',
            container: '.sp-area-image3'
        },
        {
            dataKey: 'text',
            blockName: 'text',
            container: '.sp-area2'
        }
    ];

    this.getData = function () {
        return data;
    };

    this.collectData = function () {
        data.valueCount = $el.find('.sp-izi-count').val();
        data.valueTitle = $el.find('.sp-izi-count-title').val();
        return data;
    };

    this.getAreas = function () {
        return areas;
    };
});
