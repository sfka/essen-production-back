sprint_editor.registerBlock('my_blockquote', function ($, $el, data, settings) {

    settings = settings || {};

    data = $.extend({
        value: ''
    }, data);

    this.getData = function () {
        return data;
    };

    this.collectData = function () {
        data.value = $el.find('.sp-textarea').val();
        return data;
    };

    this.afterRender = function () {
        var $input = data.value = $el.find('.sp-textarea');
    };
});
