sprint_editor.registerBlock('my_directions_numbers', function ($, $el, data) {

    this.getData = function () {
        return data;
    };

    this.collectData = function () {
        data.items = [];
        $('.sp-izi-numbers-container .counter').each(function () {
            var item = {};
            item['number'] = $(this).find('.counter__num').val();
            item['title'] = $(this).find('.counter__title').val();
            item['description'] = $(this).find('.counter__text').val();
            data.items.push(item);
        });

        return data;
    };
    this.afterRender = function () {
        if (data.items) {
            $.each(data.items, function (index, item) {
                addRow(item);
            });
        }

        $el.on('click', '.sp-izi-add-number-row', function (e) {
            addRow({
                number: '',
                title: '',
                description: '',
            });
        });
    }

    function addRow(rowData) {
        $rowHtml =
            '<div class="counter">\
                <input placeholder="Цифра" class="counter__num" value="' + rowData['number'] + '">\
                <input placeholder="Заголовок" class="counter__title" value="' + rowData['title'] + '">\
                <input placeholder="Описание" class="counter__text" value="' + rowData['description'] + '">\
            </div>';
        $('.sp-izi-numbers-container').append($rowHtml);
    }
});
