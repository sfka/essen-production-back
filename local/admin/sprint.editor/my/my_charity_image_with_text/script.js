sprint_editor.registerBlock('my_charity_image_with_text', function ($, $el, data) {
    var areas = [
        {
            dataKey: 'image1',
            blockName: 'image',
            container: '.sp-area-image1'
        },
        {
            dataKey: 'image2',
            blockName: 'image',
            container: '.sp-area-image2'
        },
        {
            dataKey: 'text',
            blockName: 'text',
            container: '.sp-area2'
        }
    ];

    this.getData = function () {
        return data;
    };

    this.collectData = function () {
        return data;
    };

    this.getAreas = function () {
        return areas;
    };
});
