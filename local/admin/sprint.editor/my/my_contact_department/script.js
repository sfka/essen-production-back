sprint_editor.registerBlock('my_contact_department', function ($, $el, data) {

    this.getData = function () {
        return data;
    };

    this.collectData = function () {
        data.items = [];
        $('.sp-izi-contacts-container .department').each(function () {
            var item = {};
            item['number'] = $(this).find('.contacts__department').val();
            item['phone'] = {};
            item['email'] = {};
            $(this).find('.department__contacts_row_phone .contacts__phone').each(function (index, value) {
                item['phone'][index] = {};
                item['phone'][index] = $(this).val();
            });
            $(this).find('.department__contacts_row_email .contacts__email').each(function (index, value) {
                item['email'][index] = {};
                item['email'][index] = $(this).val();
            });
            data.items.push(item);
        });
        return data;
    };

    this.afterRender = function () {
        if (data.items) {
            $.each(data.items, function (index, item) {
                addRow(item);
            });
        }

        $el.on('click', '.sp-izi-add-contacts-row', function (e) {
            addRow({
                number: '',
            });
        });
        $el.on('click', '.department__contacts_add', function (e) {
            var type = $(this).attr('data-type');
            switch (type) {
                case '_phone':
                    var newRow = '<input placeholder="Телефон" class="contacts__phone" value="">';
                    break;

                case '_email':
                    var newRow = '<input placeholder="Email" class="contacts__email" value="">';
                    break;
            }
            $(this).closest('.department__contacts').find('.department__contacts_row' + type).append(newRow);
        });
    };

    function addRow(rowData) {
        $contactsRowPhone = '';
        $contactsRowEmail = '';
        if (typeof rowData.phone == 'object') {
            $.each(rowData.phone, function (index, value) {
                $contactsRowPhone +=
                    '<input placeholder="Телефон" class="contacts__phone" value="' + value + '">';
            });
        }
        if (typeof rowData.email == 'object') {
            $.each(rowData.email, function (index, value) {
                $contactsRowEmail +=
                    '<input placeholder="Email" class="contacts__email" value="' + value + '">';
            });
        }
        if(typeof rowData.phone != 'object' && typeof rowData.email != 'object') {
            $contactsRowPhone =
                '<div class="department__contacts_row_phone">\
                    <input placeholder="Телефон" class="contacts__phone" value="">\
                </div>';
            $contactsRowEmail =
                '<div class="department__contacts_row_email">\
                    <input placeholder="Email" class="contacts__email" value="">\
                </div>';
        }
        $rowHtml =
            '<div class="department">\
                <input placeholder="Отдел" class="contacts__department" value="' + rowData['number'] + '">\
                <div class="department__contacts">\
                    <input class="department__contacts_add" data-type="_phone" type="button" value="Добавить телефон">\
                    <input class="department__contacts_add" data-type="_email" type="button" value="Добавить email">\
                    <div class="department__contacts_row_phone">' + $contactsRowPhone + '</div>\
                    <div class="department__contacts_row_email">' + $contactsRowEmail + '</div>\
                </div>\
            </div>';
        $('.sp-izi-contacts-container').append($rowHtml);
    }
});
