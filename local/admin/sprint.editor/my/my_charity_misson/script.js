sprint_editor.registerBlock('my_charity_misson', function ($, $el, data) {
    var areas = [
        {
            dataKey: 'image-left',
            blockName: 'image',
            container: '.sp-area-image-left'
        },
        {
            dataKey: 'image-right',
            blockName: 'image',
            container: '.sp-area-image-right'
        },
        {
            dataKey: 'image-light',
            blockName: 'image',
            container: '.sp-area-image-light'
        },
        {
            dataKey: 'image-dark',
            blockName: 'image',
            container: '.sp-area-image-dark'
        },
        {
            dataKey: 'text',
            blockName: 'text',
            container: '.sp-area2'
        }
    ];

    this.getData = function () {
        return data;
    };

    this.collectData = function () {
        return data;
    };

    this.getAreas = function () {
        return areas;
    };
});
