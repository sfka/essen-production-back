$(document).ready(function () {
    initAjaxFilter();
    initAjaxModalList();
    initAjaxScrollModal();
    initAjaxScrollSlider();
    initAjaxModalDetail();
    initForms();
});

function initAjaxFilter() {
    $('body').on('click', '.ajax__filter_elements', function (event) {
        event.preventDefault();
        var propCode = $(this).attr('data-prop');
        var propValue = $(this).attr('data-value');
        var data = {};
        var selectedData = JSON.parse($('.js__filter').attr('data-filter'));
        var block_id = '.ajax__update_content';

        $(block_id).fadeTo(200, 0.1);
        data['PROPERTY_' + propCode] = propValue;

        if (selectedData != null) {
            data = $.extend(selectedData, data);
        }

        $.ajax({
            type: "GET",
            url: window.location.href,
            data: data,
            beforeSend: function () {
                $('.filter-close').trigger('click');
            },
            success: function (data) {
                var $text = $(data).html();
                $(block_id).html($text);
                $(block_id).fadeTo(200, 1);
                itemsSlider();
                teamSlider();
                initSlides();
                initHorizontalScrollableList();
            }
        });
    });
}

function initAjaxModalList() {
    $('body').on('click', '.ajax__modal_list', function () {
        var blockID = $(this).attr('href');
        if ($.trim($(blockID).find('.ajax__update_content').html()) == '') {
            var data = {};
            data['AJAX_MODAL'] = 'Y';
            $.ajax({
                type: "GET",
                url: window.location.href,
                data: data,
                beforeSend: function () {
                    //$(blockID).fadeTo(200, 0.1);
                },
                success: function (data) {
                    var $text = $(data).find('.ajax__update_content').html();
                    $(blockID).find('.ajax__update_content').html($text);
                    //$(blockID).fadeTo(200, 1);
                    tiltCards();
                    togglePopup();
                    initHorizontalScrollableList();
                }
            });
        }
    });
}

function initAjaxScrollModal() {
    $('body').on('mousewheel', '.ajax__scroll_modal[data-script="true"]', function () {
        let scriptEnable = true;
        if (this.scrollHeight == this.scrollTop + $(this).outerHeight() && scriptEnable === true) {
            var elem = $(this);
            elem.attr('data-script', 'false');
            scriptEnable = elem.attr('data-script');
            var page = parseInt(elem.attr('data-page'));
            var pagen = elem.attr('data-pagen');
            var allPage = parseInt(elem.attr('data-all-page'));
            if ((page + 1) <= allPage) {
                var blockID = elem.find('.ajax__scroll_row');

                var data = {};
                data['AJAX_MODAL'] = 'Y';
                data['PAGEN_' + pagen] = page + 1;
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    data: data,
                    beforeSend: function () {
                        blockID.fadeTo(200, 0.1);
                    },
                    success: function (data) {
                        elem.attr('data-page', page + 1);
                        if (elem.hasClass('ajax__scroll_modal')) {
                            var $text = $(data).find('.ajax__scroll_modal .ajax__scroll_row').html();
                        }
                        if (elem.hasClass('ajax__scroll_slider')) {
                            var $text = $(data).find('.ajax__scroll_slider .ajax__scroll_row').html();
                        }
                        blockID.append($text);
                        blockID.fadeTo(200, 1);
                        scriptEnable = true;
                        elem.animate({
                            scrollTop: elem.outerHeight()
                        }, 100);
                        if ((page + 1) == allPage) {
                            scriptEnable = false;
                        } else if ((page + 1) < allPage) {
                            scriptEnable = true;
                        }
                        elem.attr('data-script', scriptEnable);
                        tiltCards();
                        togglePopup();
                        initHorizontalScrollableList();
                    },
                });
            }
        }
    });
}

function initAjaxScrollSlider() {
    $('body').on('mousewheel', '.ajax__scroll_slider[data-script="true"]', function () {
        let scriptEnable = true;
        if (this.scrollHeight == this.scrollTop + $(this).outerHeight() && scriptEnable === true) {
            var elem = $(this);
            elem.attr('data-script', 'false');
            scriptEnable = elem.attr('data-script');
            var page = parseInt(elem.attr('data-page'));
            var pagen = elem.attr('data-pagen');
            var allPage = parseInt(elem.attr('data-all-page'));
            var filter = elem.attr('data-filter');
            if ((page + 1) <= allPage) {
                var blockID = elem.find('.ajax__scroll_row');

                var data = {};
                data['AJAX_MODAL'] = 'Y';
                data['PAGEN_' + pagen] = page + 1;

                if (typeof filter != 'undefined') {
                    var obFilter = JSON.parse(filter);
                    for (var key in obFilter) {
                        data[key] = obFilter[key];
                    }
                }
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    data: data,
                    beforeSend: function () {
                        blockID.fadeTo(200, 0.1);
                    },
                    success: function (data) {
                        elem.attr('data-page', page + 1);
                        if (elem.hasClass('ajax__scroll_modal')) {
                            var $text = $(data).find('.ajax__scroll_modal .ajax__scroll_row').html();
                        }
                        if (elem.hasClass('ajax__scroll_slider')) {
                            var $text = $(data).find('.ajax__scroll_slider .ajax__scroll_row').html();
                        }
                        blockID.append($text);
                        blockID.fadeTo(200, 1);
                        scriptEnable = true;
                        elem.animate({
                            scrollTop: elem.outerHeight()
                        }, 100);
                        if ((page + 1) == allPage) {
                            //elem.find('.ajax__scroll_row').attr('data-count', 'all');
                            scriptEnable = false;
                        } else if ((page + 1) < allPage) {
                            scriptEnable = true;
                        }
                        elem.attr('data-script', scriptEnable);
                        tiltCards();
                        togglePopup();
                        initHorizontalScrollableList();
                    },
                });
            }
        }
    });
}

function initAjaxModalDetail() {
    $('body').on('click', '.ajax__vacancy_modal', function () {
        var elem = $(this);
        var blockID = elem.attr('href');
        var data = {};
        data['AJAX_MODAL'] = 'Y';
        data['VACANCY_ID'] = elem.attr('data-vacancy');
        $.ajax({
            type: "GET",
            url: window.location.href,
            data: data,
            beforeSend: function () {
                //$(blockID).fadeTo(200, 0.1);
            },
            success: function (data) {
                if (typeof $(data).find(blockID).find('.ajax__update_content').html() != 'undefined') {
                    var $text = $(data).find(blockID).find('.ajax__update_content').html();
                } else {
                    var $text = $(data).find('.ajax__update_content').html();
                }
                $(blockID).find('.ajax__update_content').html($text);
                //$(blockID).fadeTo(200, 1);
                tiltCards();
                togglePopup();
                formValidation();
            }
        });
    });
}

function initForms() {
    var body = $('body');
    body.on('submit', '.ajax__form', function (event) {
        console.log('asd');
        event.preventDefault();
        var elem = $(this);
        var url = elem.attr('action');
        var email = elem.find('input[type=email]').val();
        if (email === undefined) {
            email = 'test@test.ru';
        }
        var data = elem.serialize();
        formValidation();
        if (elem.hasClass('form__file')) {
            data = new FormData(elem[0]);
            $.ajax({
                type: "POST",
                cache: false,
                processData: false,
                contentType: false,
                url: url,
                data: data,
                dataType: "json",
                beforeSend: function () {
                },
                success: function (res) {
                    if (res['status'] === 'ok') {
                        elem.trigger('reset');
                        $('#popupSuccess').addClass('popup-wrapper_active');
                        togglePopup();
                    }
                }
            });
        } else {
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                beforeSend: function () {
                },
                success: function (res) {
                    if (res['status'] === 'ok') {
                        elem.trigger('reset');
                        $('#popupSuccess').addClass('popup-wrapper_active');
                        togglePopup();
                    }
                }
            });
        }
    });
}