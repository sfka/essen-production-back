<header class="header <?= $arParams['HEADER_CLASS'] ?>">
    <div class="header__overlay"></div>
    <div class="header__inner">
        <? if ($arParams['TITLE_IN_HEADER'] == 'Y'): ?>
            <div class="header__title">
                <h4><?= $APPLICATION->ShowTitle() ?></h4>
            </div>
        <? endif; ?>
        <a class="header__logo-link magnetic-wrap" href="<?= SITE_DIR ?>">
            <div class="header__logo">
                <img src="<?= \CFile::GetPath(\Bitrix\Main\Config\Option::get("askaron.settings", "UF_LOGO")); ?>"
                     alt=""/>
            </div>
        </a>
        <a class="header__return" href="<?= SITE_DIR ?>">
            <svg class="header__return-icon" width="15" height="10">
                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-left"></use>
            </svg>
            <span class="header__return-text" data-text="<?= GetMessage('MAIN_PAGE') ?>"><?= GetMessage('MAIN_PAGE') ?></span>
        </a>
        <div class="header__main">

            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "top_menu",
                [
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "2",
                    "MENU_CACHE_GET_VARS" => [
                    ],
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "top",
                    "USE_EXT" => "Y",
                    "COMPONENT_TEMPLATE" => "top_menu",
                ],
                false
            ); ?>
            <div class="header__main-controls">
                <button class="theme-switch">
                    <div class="theme-switch__track">
                        <div class="theme-switch__track-item"></div>
                    </div>
                    <div class="theme-switch__label" data-theme-type="light" data-text="<?= GetMessage('LIGHT_THEME') ?>"></div>
                    <div class="theme-switch__label" data-theme-type="dark" data-text="<?= GetMessage('DARK_THEME') ?>"></div>
                </button>
                <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/system/sites_list.php', [], ['SHOW_BORDER' => false]) ?>
            </div>
        </div>
    </div>
    <div class="burger magnetic-wrap">
        <div class="burger__line"></div>
        <div class="burger__line"></div>
        <div class="burger__line"></div>
    </div>
    <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/system/megamenu.php') ?>
</header>