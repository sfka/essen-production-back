<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$rsSites = CSite::GetList($by = "sort", $order = "asc");
while ($arSite = $rsSites->Fetch()) {
    $siteList[$arSite['ID']] = $arSite;
}
?>
<div class="lang-nav">
    <div class="lang-nav__current">
        <a class="lang-nav__item"
           href="javascript:void(0);"
           lang="<?= $siteList[SITE_ID]['LANGUAGE_ID'] ?>"><?= $siteList[SITE_ID]['NAME'] ?></a>
        <svg class="lang-nav__icon" width="12" height="7">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#check"></use>
        </svg>
    </div>
    <div class="lang-nav__list">
        <? foreach ($siteList as $key => $site): ?>
            <? if ($key != SITE_ID): ?>
                <a href="<?= $site['DIR'] ?>" class="lang-nav__item"
                   lang="<?= $site['LANGUAGE_ID'] ?>"><?= $site['NAME'] ?></a>
            <? endif; ?>
        <? endforeach; ?>
        <? /*<abbr class="lang-nav__item" lang="中國">中國</abbr>*/ ?>
    </div>
</div>