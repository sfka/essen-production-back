<div class="megamenu">
    <div class="megamenu__inner">
        <a class="header__logo-link magnetic-wrap" href="<?= SITE_DIR ?>">
            <div class="header__logo">
                <img class="lazy" data-src="<?= CFile::GetPath( \Bitrix\Main\Config\Option::get( "askaron.settings", "UF_LOGO")) ?>" alt="" />
            </div>
        </a>
        <div class="creator megamenu__creator">
            <div class="creator-text"><?= GetMessage('DEVELOPMENT') ?></div>
            <a class="creator-logo creator-logo_white" href="https://ilartech.com/" target="_blank"
               style="background-image: url(<?= SITE_STYLE_PATH ?>/img/minified-svg/ilar-pro-white.svg)"></a>
            <a class="creator-logo creator-logo_black" href="https://ilartech.com/" target="_blank"
               style="background-image: url(<?= SITE_STYLE_PATH ?>/img/minified-svg/ilar-pro-black.svg)"></a>
        </div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "mega_menu",
            [
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "DELAY" => "N",
                "MAX_LEVEL" => "2",
                "MENU_CACHE_GET_VARS" => [
                ],
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "top",
                "USE_EXT" => "Y",
                "COMPONENT_TEMPLATE" => "mega_menu",
            ],
            false
        ); ?>
    </div>
</div>