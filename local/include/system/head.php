<?

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

Asset::getInstance()->addString('<meta charset="utf-8">');
Asset::getInstance()->addString('<meta http-equiv="x-ua-compatible" content="ie=edge">');
Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">');
Asset::getInstance()->addString('<meta name="format-detection" content="telephone=no">');
Asset::getInstance()->addString('<meta name="HandheldFriendly" content="true">');
Asset::getInstance()->addString('<meta name="theme-color" content="#ffffff">');
Asset::getInstance()->addString('<meta name="msapplication-navbutton-color" content="#ffffff">');
Asset::getInstance()->addString('<meta name="apple-mobile-web-app-status-bar-style" content="#ffffff">');
?>
<title><? $APPLICATION->ShowTitle(); ?></title>

<!--[if (gt IE 9)|!(IE)]><!-->
<? Asset::getInstance()->addCss(SITE_STYLE_PATH . '/css/main.min.css') ?>
<? Asset::getInstance()->addCss(SITE_STYLE_PATH . '/css/backend.css') ?>
<!--<![endif]-->
<?
Asset::getInstance()->addString('<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">');
Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="180x180" href="' . SITE_STYLE_PATH . '/favicon/apple-touch-icon.png">');
Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="32x32" href="' . SITE_STYLE_PATH . '/favicon/favicon-32x32.png">');
Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="16x16" href="' . SITE_STYLE_PATH . '/favicon/favicon-16x16.png">');
Asset::getInstance()->addString('<link rel="mask-icon" href="' . SITE_STYLE_PATH . '/favicon/safari-pinned-tab.svg" color="#FFF">');
Asset::getInstance()->addString('<link rel="manifest" href="/site.webmanifest">');

Asset::getInstance()->addJs(SITE_STYLE_PATH . '/js/separate-js/jquery.min.js');
Asset::getInstance()->addJs(SITE_STYLE_PATH . '/js/main.min.js');
Asset::getInstance()->addJs(SITE_STYLE_PATH . '/js/backend.js');
?>
<script>
    (function () {
        var $h, t, p;
        $h = document.documentElement;
        t = window.matchMedia('(prefers-color-scheme:dark)').matches ? 'dark' : 'light';
        p = localStorage.getItem('theme');
        $h.setAttribute('data-theme', (p && (p === 'dark' || p === 'light')) ? p : t)
    }());
</script>
<style>
    .loading {
        opacity: 0
    }

    .layer, .megamenu, .popup-wrapper, .filter {
        display: none
    }

    .wow {
        visibility: hidden
    }
</style>