
<? // пресс кит ?>
<div class="popup-wrapper" id="popupPressKit">
    <div class="popup-wrapper__close popup-close magnetic-wrap">
        <svg class="popup-wrapper__close-icon" width="22" height="22">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </div>
    <div class="popup-wrapper__bg">
        <div class="popup-wrapper__bg-left"></div>
        <div class="popup-wrapper__bg-right"></div>
    </div>
    <div class="popup-wrapper__inner">
        <? $APPLICATION->IncludeFile(SITE_INCLUDE_PATH . '/components/presskit.php', [], ['SHOW_BORDER' => false]) ?>
    </div>
</div>
<? // Адреса производственных площадок ?>
<? $APPLICATION->IncludeComponent(
    "bitrix:news.detail",
    "contacts_address",
    [
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_ELEMENT_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "Y",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "BROWSER_TITLE" => "-",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "ELEMENT_CODE" => "",
        "ELEMENT_ID" => 51,
        "FIELD_CODE" => ["", ""],
        "IBLOCK_ID" => "18",
        "IBLOCK_TYPE" => "contacts",
        "IBLOCK_URL" => "",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "MESSAGE_404" => "",
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Страница",
        "PROPERTY_CODE" => ["NAME_EN", "NAME_CH", "ADDRESS", "ADDRESS_EN",
            "ADDRESS_CH", "PRODUCTION_ADDRESSES", "PRODUCTION_ADDRESSES_EN",
            "PRODUCTION_ADDRESSES_CH"],
        "SET_BROWSER_TITLE" => "N",
        "SET_CANONICAL_URL" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "STRICT_SECTION_CHECK" => "N",
        "USE_PERMISSIONS" => "N",
        "USE_SHARE" => "N",
    ]
); ?>
<? // Форма в контактах ?>
<div class="popup-wrapper" id="popupContactsForm">
    <div class="popup-wrapper__close popup-close magnetic-wrap magnetic-wrap">
        <svg class="popup-wrapper__close-icon" width="22" height="22">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </div>
    <div class="popup-wrapper__bg">
        <div class="popup-wrapper__bg-left"></div>
        <div class="popup-wrapper__bg-right"></div>
    </div>
    <div class="popup-wrapper__inner">
        <div class="popup-wrapper__left-col">
            <div class="content-container">
                <div class="popup-brand__wrapper popup-brand__wrapper_full">
                    <div class="popup-brand popup-brand_big">
                        <img class="lazy" data-src="<?= CFile::GetPath(\Bitrix\Main\Config\Option::get("askaron.settings", "UF_LOGO")); ?>"
                             alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-wrapper__right-col">
            <form class="default-form ajax__form"
                  action="<?= SITE_AJAX_PATH ?>/form_contacts.php" method="post"
                  enctype="multipart/form-data">
                <h2><?= GetMessage('FEEDBACK') ?></h2>
                <div class="max-width">
                    <p class="default-form__text"><?= GetMessage('FEEDBACK_CONTACT_DESCRIPTION') ?></p>
                    <div class="form-group">
                        <div class="form-inputs">
                            <div class="form-input">
                                <input class="form-control" placeholder="" id="inputName" name="name">
                                <label class="form-input__label" for="inputName"><?= GetMessage('NAME') ?></label>
                            </div>
                            <div class="form-input">
                                <input class="form-control phone-mask" type="tel" placeholder="" id="inputPhone"
                                       name="phone">
                                <label class="form-input__label" for="inputPhone"><?= GetMessage('PHONE') ?> *</label>
                            </div>
                            <div class="form-input">
                                <input class="form-control" type="email" placeholder="" id="inputEmail" name="email">
                                <label class="form-input__label" for="inputEmail"><?= GetMessage('EMAIL') ?> *</label>
                            </div>
                            <div class="form-input">
                                <input class="form-control" placeholder="" id="inputMessage" name="message">
                                <label class="form-input__label" for="inputMessage"><?= GetMessage('MESSAGE') ?></label>
                            </div>
                        </div>
                        <div class="form-btn">
                            <button class="main-button btn">
                                <span class="main-button__text"><?= GetMessage('SEND') ?></span>
                            </button>
                        </div>
                    </div>
                    <p class="default-form__text"><?= GetMessage('AGREEMENT') ?></p>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="popup-wrapper" id="popupSuccess">
    <div class="popup-wrapper__close popup-close magnetic-wrap magnetic-wrap">
        <svg class="popup-wrapper__close-icon" width="22" height="22">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#close"></use>
        </svg>
    </div>
    <div class="popup-wrapper__bg">
        <div class="popup-wrapper__bg-left"></div>
        <div class="popup-wrapper__bg-right"></div>
    </div>
    <div class="popup-wrapper__inner">
        <div class="popup-wrapper__left-col">
            <div class="content-container">
                <div class="popup-brand__wrapper popup-brand__wrapper_full">
                    <div class="popup-brand popup-brand_small">
                        <img src="<?= SITE_STYLE_PATH ?>/img/general/brand-light.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-wrapper__right-col">
            <div class="content-container">
                <div class="popup-success__text">
                    <div class="popup-success__text-inner">
                        <h2><?= GetMessage('THX_APPEAL') ?></h2>
                        <p class="default-form__text"><?= GetMessage('MANAGER_CONTACT_YOU') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>