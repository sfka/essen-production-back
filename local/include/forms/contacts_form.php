<a class="page__link popup-open" href="#popupContactsForm">
    <div class="page__link-icon-wrapper">
        <svg class="page__link-icon" width="14" height="14">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#callback"></use>
        </svg>
    </div>
    <span class="page__link-text" data-text="<?= GetMessage('FEEDBACK') ?>"><?= GetMessage('FEEDBACK') ?></span>
</a>
