<div class="popup-wrapper__right-col">
    <form class="default-form ajax__form form__file"
          novalidate="novalidate"
          action="<?= SITE_AJAX_PATH ?>/form_vacancy.php" method="post"
          enctype="multipart/form-data">
        <input type="hidden" name="vacancy" value="<?= $arParams['VACANCY'] ?>">
        <? $arTypeFile = \CIBlockProperty::GetByID(173, IBLOCK_FORM_VACANCY)->Fetch(); ?>
        <input type="hidden" name="type_file" value="<?= $arTypeFile['FILE_TYPE'] ?>">
        <h2><?= GetMessage('VACANCY_RESPONSE') ?></h2>
        <div class="max-width">
            <p class="default-form__text"><?= GetMessage('DOWNLOAD_QUESTIONARY') ?>:</p>
            <div class="form-group">
                <div class="form-inputs">
                    <div class="form-input">
                        <input class="form-control" placeholder="" id="input1" name="surname">
                        <label class="form-input__label" for="input1"><?= GetMessage('SURNAME') ?></label>
                    </div>
                    <div class="form-input">
                        <input class="form-control" placeholder="" id="inputName" name="name">
                        <label class="form-input__label" for="inputName"><?= GetMessage('NAME') ?></label>
                    </div>
                    <div class="form-input">
                        <input class="form-control" placeholder="" id="input3" name="patronymic">
                        <label class="form-input__label" for="input3"><?= GetMessage('PATRONYMIC') ?></label>
                    </div>
                    <div class="form-input">
                        <input class="form-control birthday-mask" placeholder="" id="input4" name="date">
                        <label class="form-input__label" for="input4"><?= GetMessage('BIRTHDAY') ?></label>
                    </div>
                    <div class="form-input">
                        <input class="form-control" type="email" placeholder="" id="inputEmail" name="email">
                        <label class="form-input__label" for="inputEmail"><?= GetMessage('EMAIL') ?> *</label>
                    </div>
                    <div class="form-input">
                        <input class="form-control phone-mask" type="tel" placeholder="" id="inputPhone"
                               name="phone">
                        <label class="form-input__label" for="inputPhone"><?= GetMessage('PHONE') ?> *</label>
                    </div>
                </div>
                <label class="form-file" for="formFile">
                    <input type="hidden">
                    <input type="file" name="file" id="formFile">
                    <div class="form-file__icon">
                        <div class="form-file__icon-show">
                            <svg class="icon" width="27" height="15">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#file"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="form-file__icon-hide">
                        <div class="form-file__close">
                            <svg class="icon" width="10" height="10">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#file-close"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="form-file__text"
                         data-text="<?= GetMessage('FILE_QUESTIONARY') ?>"><?= GetMessage('FILE_QUESTIONARY') ?></div>
                </label>
                <div class="form-btn">
                    <button class="main-button btn">
                        <span class="main-button__text"><?= GetMessage('RESPOND') ?></span>
                    </button>
                </div>
            </div>
            <p class="default-form__text"><?= GetMessage('AGREEMENT') ?></p>
        </div>
    </form>
</div>