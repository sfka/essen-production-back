<div class="layer">
    <div class="layer__inner">
        <div class="layer__logo">
            <div class="layer__logo-img"
                 style="background-image: url(<?= \CFile::GetPath(\Bitrix\Main\Config\Option::get("askaron.settings", "UF_LOGO")); ?>)"></div>
        </div>
    </div>
</div>