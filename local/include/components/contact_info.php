
<? $arResult['CONTACT_INFO'] = $arParams['CONTACT_INFO'] ?>
<div class="contacts offset-big">
    <div class="contacts__top">
        <h2><?= GetMessage('CONTACT_INFO') ?></h2>
        <p>
            <i><?= (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['NAME'] : $arResult['CONTACT_INFO']['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]) ?></i>
        </p>
    </div>
    <div class="contacts__row">
        <div class="contacts__column">
            <? if ($arResult['CONTACT_INFO']['PROPERTIES']['LOGOS']['VALUE']): ?>
                <div class="contacts__brands">
                    <? foreach ($arResult['CONTACT_INFO']['PROPERTIES']['LOGOS']['VALUE'] as $key => $logo): ?>
                        <div class="item-brand">
                            <img class="lazy" data-src="<?= CFile::GetPath($logo) ?>"
                                 alt="<?= $arResult['CONTACT_INFO']['PROPERTIES']['LOGOS']['DESCRIPTION'][$key] ?>">
                        </div>
                    <? endforeach; ?>
                </div>
            <? endif; ?>
            <? $previewProp = (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['PROPERTIES']['PREVIEW'] : $arResult['CONTACT_INFO']['PROPERTIES']['PREVIEW_' . strtoupper(SITE_ID)]) ?>
            <? if ($previewProp['~VALUE']['TEXT']): ?>
                <div class="contacts__text">
                    <?= $previewProp['~VALUE']['TEXT']; ?>
                </div>
            <? endif; ?>
            <? $requisitesProp = (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['PROPERTIES']['REQUISITES'] : $arResult['CONTACT_INFO']['PROPERTIES']['REQUISITES_' . strtoupper(SITE_ID)]) ?>
            <? if ($requisitesProp['VALUE']): ?>
                <div class="contacts__data">
                    <? foreach ($requisitesProp['VALUE'] as $key => $val): ?>
                        <p><?= $val ?> <?= $requisitesProp['DESCRIPTION'][$key] ?></p>
                    <? endforeach; ?>
                </div>
            <? endif; ?>
        </div>
        <div class="contacts__column">
            <div class="contacts-items">
                <? $phone = (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['PROPERTIES']['PHONE'] : $arResult['CONTACT_INFO']['PROPERTIES']['PHONE_' . strtoupper(SITE_ID)]) ?>
                <? if ($phone['VALUE']): ?>
                    <div class="contacts-item phone" data-size="big">
                        <div class="icon-item">
                            <svg class="icon" width="28" height="28">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#phone"></use>
                            </svg>
                        </div>
                        <div class="info-item"><span
                                    class="label-item"><?= GetMessage('PHONE') ?>:</span>
                            <p class="desc-item">
                                <a href="tel:<?= preg_replace('/[^\d+]+/', '', $phone['VALUE']); ?>"><?= $phone['VALUE'] ?></a>
                            </p>
                        </div>
                    </div>
                <? endif; ?>
                <? $email = (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['PROPERTIES']['EMAIL'] : $arResult['CONTACT_INFO']['PROPERTIES']['EMAIL_' . strtoupper(SITE_ID)]) ?>
                <? if ($email['VALUE']): ?>
                    <div class="contacts-item email" data-size="big">
                        <div class="icon-item">
                            <svg class="icon" width="29" height="24">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#email"></use>
                            </svg>

                        </div>
                        <div class="info-item">
                            <span class="label-item"><?= GetMessage('EMAIL') ?>:</span>
                            <p class="desc-item">
                                <a href="mailto:<?= $email['VALUE'] ?>"><?= $email['VALUE'] ?></a>
                            </p>
                        </div>
                    </div>
                <? endif; ?>
                <? $address = (SITE_ID == 's1' ? $arResult['CONTACT_INFO']['PROPERTIES']['ADDRESS'] : $arResult['CONTACT_INFO']['PROPERTIES']['ADDRESS_' . strtoupper(SITE_ID)]) ?>
                <? if ($address['VALUE']): ?>
                    <div class="contacts-item address" data-size="big">
                        <div class="icon-item">
                            <svg class="icon" width="22" height="31">
                                <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#address"></use>
                            </svg>
                        </div>
                        <div class="info-item">
                            <span class="label-item"><?= GetMessage('ADDRESS') ?>:</span>
                            <p class="desc-item"><?= $address['VALUE'] ?></p>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
</div>