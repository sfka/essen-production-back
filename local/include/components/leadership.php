<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();
$ajax = $request->isAjaxRequest(); ?>

<? $arResult['LEADERSHIP'] = $arParams['LEADERSHIP'] ?>
    <div class="team-slider offset-small">
        <div class="team-slider__content">
            <? foreach ($arResult['LEADERSHIP'] as $arItem): ?>
                <? $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
                $position = (SITE_ID == 's1' ? $arItem['PROPERTIES']['POSITION']['VALUE'] : $arItem['PROPERTIES']['POSITION_' . strtoupper(SITE_ID)]['VALUE']);
                $contacts = (SITE_ID == 's1' ? $arItem['PROPERTIES']['CONTACTS'] : $arItem['PROPERTIES']['CONTACTS_' . strtoupper(SITE_ID)]);
                $phone = $arItem['PROPERTIES']['PHONE']['VALUE'];
                $mail = $arItem['PROPERTIES']['EMAIL']['VALUE'];
                $prevText = (SITE_ID == 's1' ? $arItem['PREVIEW_TEXT'] : $arItem['PROPERTIES']['PREVIEW_TEXT_' . strtoupper(SITE_ID)]['~VALUE']['TEXT']);
                $biography = (SITE_ID == 's1' ? $arItem['PROPERTIES']['BIOGRAPHY']['~VALUE']['TEXT'] : $arItem['PROPERTIES']['BIOGRAPHY_' . strtoupper(SITE_ID)]['VALUE']);
                ?>
                <div class="item-content">
                    <div class="text">
                        <h2><?= $name ?></h2><span><?= $position ?></span>
                        <p><?= $prevText ?></p>
                        <div class="offset-content">
                            <h6><?= GetMessage('CONTACTS') ?>:</h6>
                            <ul>
                                <? foreach ($contacts['VALUE'] as $key => $contact): ?>
                                    <li><?= $contact ?>: <?= $contacts['DESCRIPTION'][$key] ?></li>
                                <? endforeach; ?>
                                <li><?= GetMessage('PHONE') ?>: <a href="tel:<?= preg_replace('/[^\d+]+/', '', $phone); ?>"><?= $phone ?></a></li>
                                <li><?= GetMessage('EMAIL') ?>: <a href="mailto:<?= $mail ?>"><?= $mail ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="offset-content">
                        <a class="more-link popup-open ajax__vacancy_modal" href="#popupTeam" data-text="<?= GetMessage('MORE_BIOGRAPHY') ?>"
                           data-type="leadership" data-vacancy="<?= $arItem['ID'] ?>"><?= GetMessage('MORE_BIOGRAPHY') ?></a>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
        <div class="team-slider__items swiper-container">
            <div class="team-slider__wrap swiper-wrapper">
                <? foreach ($arResult['LEADERSHIP'] as $arItem): ?>
                    <? $name = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
                    $position = (SITE_ID == 's1' ? $arItem['PROPERTIES']['POSITION']['VALUE'] : $arItem['PROPERTIES']['POSITION_' . strtoupper(SITE_ID)]['VALUE']);
                    $contacts = (SITE_ID == 's1' ? $arItem['PROPERTIES']['CONTACTS'] : $arItem['PROPERTIES']['CONTACTS_' . strtoupper(SITE_ID)]);
                    $phone = (SITE_ID == 's1' ? $arItem['PROPERTIES']['PHONE']['VALUE'] : $arItem['PROPERTIES']['PHONE_' . strtoupper(SITE_ID)]['VALUE']);
                    $mail = (SITE_ID == 's1' ? $arItem['PROPERTIES']['EMAIL']['VALUE'] : $arItem['PROPERTIES']['EMAIL_' . strtoupper(SITE_ID)]['VALUE']);
                    $prevText = (SITE_ID == 's1' ? $arItem['PREVIEW_TEXT'] : $arItem['PROPERTIES']['PREVIEW_TEXT_' . strtoupper(SITE_ID)]['~VALUE']['TEXT']);
                    $biography = (SITE_ID == 's1' ? $arItem['PROPERTIES']['BIOGRAPHY']['~VALUE']['TEXT'] : $arItem['PROPERTIES']['BIOGRAPHY_' . strtoupper(SITE_ID)]['VALUE']);
                    ?>
                    <div class="column-wrap swiper-slide <?=(!next($array) ? 'swiper-slide-init' : false)?>">
                        <div class="team-slider__item">
                            <img class="lazy" data-src="<?= CFile::GetPath($arItem['PREVIEW_PICTURE']); ?>" alt="<?= $name ?>">
                        </div>
                        <div class="team-slider__name"><?= $name ?></div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <div class="team-slider__controls">
            <div class="slider-controls">
                <div class="team-slider__btn team-slider__btn-prev slider-btn">
                    <svg class="slider-btn__icon" width="18" height="12">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-left"></use>
                    </svg>
                </div>
                <div class="team-slider__btn team-slider__btn-next slider-btn">
                    <svg class="slider-btn__icon" width="18" height="12">
                        <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#arrow-right"></use>
                    </svg>
                </div>
            </div>
        </div>
    </div>