<div class="popup-wrapper__left-col">
    <div class="content-container">
        <h2><?= GetMessage('USE_ORIGINAL_FILE') ?></h2>
        <p><?= GetMessage('USE_ORIGINAL_FILE_DESC') ?></p>
        <div class="popup-brand__wrapper">
            <div class="popup-brand popup-brand_small">
                <img src="<?= CFile::GetPath(\Bitrix\Main\Config\Option::get("askaron.settings", "UF_LOGO")); ?>">
            </div>
        </div>
    </div>
</div>
<div class="popup-wrapper__right-col">
    <div class="content-container">
        <h2><?= GetMessage('ESSEN_LOGO') ?></h2>
        <p class="default-form__text"><?= GetMessage('LOGO_VERSION_DESC') ?></p>
        <? $APPLICATION->IncludeComponent("bitrix:news.list",
            "logos",
            [
                "COMPONENT_TEMPLATE" => ".default",
                "IBLOCK_TYPE" => "widgets",
                "IBLOCK_ID" => "14",
                "NEWS_COUNT" => "4",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => [
                    0 => "",
                    1 => "",
                ],
                "PROPERTY_CODE" => [
                    0 => "NAME_EN",
                    1 => "NAME_CH",
                    2 => "FILE_EPS",
                    3 => "FILE_PNG",
                    4 => "FILE_SVG",
                    5 => "FILE_PDF",
                ],
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_LAST_MODIFIED" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "N",
                "STRICT_SECTION_CHECK" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "N",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "SET_STATUS_404" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => "",
            ],
            false
        ); ?>
    </div>
</div>