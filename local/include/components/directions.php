<? $arResult['DIRECTIONS'] = $arParams['DIRECTIONS']; ?>
    <div class="content__cite cite">
        <p>
            <i><?= $arParams['PREVIEW_TEXT'] ?></i>
        </p>
        <p><i><?= GetMessage('COMPANY_CONSISTS') ?>:</i></p>
    </div>
<? $index = 0; ?>
<? foreach ($arResult['DIRECTIONS'] as $arItem): ?>
    <? $index++;
    $dirName = (SITE_ID == 's1' ? $arItem['NAME'] : $arItem['PROPERTIES']['NAME_' . strtoupper(SITE_ID)]['VALUE']);
    $dirPrevText = (SITE_ID == 's1' ? $arItem['PREVIEW_TEXT'] : $arItem['PROPERTIES']['PREVIEW_TEXT_' . strtoupper(SITE_ID)]['~VALUE']['TEXT']);
    $dirNumText = (SITE_ID == 's1' ? $arItem['PROPERTIES']['NUMBERS_TEXT']['VALUE'] : $arItem['PROPERTIES']['NUMBERS_TEXT_' . strtoupper(SITE_ID)]['VALUE']);
    $dirNumDesc = (SITE_ID == 's1' ? $arItem['PROPERTIES']['NUMBERS_TEXT']['DESCRIPTION'] : $arItem['PROPERTIES']['NUMBERS_TEXT_' . strtoupper(SITE_ID)]['DESCRIPTION']);
    ?>
    <div class="content__type <?= ($index % 2 == 0 ? 'content__type_even' : 'content__type_odd') ?>">
        <div class="content__type-row">
            <div class="content__type-column">
                <div class="content__type-img">
                    <img class="lazy" data-src="<?= CFile::GetPath($arItem['PROPERTIES']['LOGO_DIRECTION']['VALUE']) ?>"
                         alt="<?= $dirName ?>">
                </div>
            </div>
            <div class="content__type-column">
                <div class="content__type-content">
                    <div class="content__type-label"><?= $dirName ?></div>
                    <h3><?= $dirPrevText ?></h3>
                    <div class="content__type-bottom">
                        <div class="counter">
                            <div class="counter__num"><?= $arItem['PROPERTIES']['NUMBERS']['VALUE'] ?></div>
                            <div class="counter__title"><?= strtoupper($dirNumText) ?></div>
                            <div class="counter__text"><?= $dirNumDesc ?></div>
                        </div>
                        <? if ($arItem['PROPERTIES']['LINK']['VALUE']): ?>
                            <a class="main-button main-button_site magnetic-wrap"
                               href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>">
                                <span class="main-button__text"><?= GetMessage('LINK_TO_SITE') ?></span>
                            </a>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endforeach; ?>