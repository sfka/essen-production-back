<a class="page__link popup-open" href="#popupContactsAddress">
    <div class="page__link-icon-wrapper">
        <svg class="page__link-icon" width="12" height="17">
            <use xlink:href="<?= SITE_STYLE_PATH ?>/img/general/svg-symbols.svg#map"></use>
        </svg>
    </div>
    <span class="page__link-text" data-text="<?= GetMessage('SHOW_ADDRESSES') ?>"><?= GetMessage('SHOW_ADDRESSES') ?></span>
</a>
